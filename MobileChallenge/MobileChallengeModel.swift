//
//  MobileChallengeModel.swift
//  MobileChallenge
//
//  Created by Masum Ahmed on 16/8/21.
//  Copyright © 2021 Ashraf Ul Alam Tusher. All rights reserved.
//

import Foundation
import SwiftyJSON

class MCStoreInfo
{
    var name: String?
    var rating: Double?
    var openingTime: String?
    var closingTime: String?

    init(){}

    init(fromJSON json: JSON) {
        name = json["name"].stringValue
        rating = json["rating"].doubleValue
        openingTime = json["openingTime"].stringValue
        closingTime = json["closingTime"].stringValue
    }
}


class MCProductList
{
    var list: [MCProduct]?
    
    init(){}

    init(fromJSON json: JSON) {
        list = json.arrayValue.map { MCProduct(fromJSON: $0) }
    }
}

class MCProduct
{
    var name: String?
    var price: Double?
    var imageUrl: String?
    var qty: Int?
    //var selected: Bool?

    init(){}

    init(fromJSON json: JSON) {
        name = json["name"].stringValue
        price = json["price"].doubleValue
        imageUrl = json["imageUrl"].stringValue
        qty = 1
        //selected = false
    }
    
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()

        if name != nil{
            dictionary["name"] = name
        }
        if price != nil{
            dictionary["price"] = Int(price ?? 0)
        }
        if imageUrl != nil{
            dictionary["imageUrl"] = "test url"//imageUrl
        }
        return dictionary
    }
}


