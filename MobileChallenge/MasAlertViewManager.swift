//
//  MasAlertViewManager.swift
//  Thoag
//
//  Created by Masum Ahmed on 9/12/19.
//  Copyright © 2019 Ashraf Ul Alam Tusher. All rights reserved.
//

import UIKit
import Foundation

let alertTitleOk = "ok".localized
let alertTitleCancel = "cancel".localized
let alertTitleSave = "Save"
let alertTitleNo = "no".localized
let alertTitleYes = "yes".localized

let kFieldPassword = "password"
let kFieldConfirmPassword = "confirm password"

var keyAssociatedAlertPlaceHolder : Int = 0
var keyAssociatedAactionSave : Int = 0

class MasAlertViewManager: NSObject, UITextFieldDelegate {
    
    static var singleton = MasAlertViewManager()
    
    func showAlert(target : UIViewController?, animated : Bool, message : String, handlerOK:(()->Void)? = nil) {
        
        let alertController = UIAlertController(title: "message".localized, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        let actionOK = UIAlertAction(title: alertTitleOk, style: UIAlertActionStyle.default) { (OK : UIAlertAction) in
            
            handlerOK?()
        }
        
        alertController .addAction(actionOK)
        target?.present(alertController, animated: animated, completion: nil)
    }
    
    //MARK: Display Alert With Ok Button
    class func showAlert(target : UIViewController?, animated : Bool, message : String, handlerOK:(()->Void)? = nil) {
        
        let alertController = UIAlertController(title: "message".localized, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        let actionOK = UIAlertAction(title: alertTitleOk, style: UIAlertActionStyle.default) { (OK : UIAlertAction) in
            
            handlerOK?()
        }
        
        alertController .addAction(actionOK)
        target?.present(alertController, animated: animated, completion: nil)
    }
    
    //MARK: Display Alert With Input TextFields
    class func showAlertWithInputs(target : UIViewController, animated : Bool, message : String, inputPlaceHolders:[String], handlerCancel:@escaping (()->Void), handlerSave:@escaping (()->Void)) {
        
        let alertController = UIAlertController(title: "message".localized, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        let actionCancel = UIAlertAction(title: alertTitleCancel, style: UIAlertActionStyle.default) { (CANCEL : UIAlertAction) in
            
            handlerCancel()
        }
        
        let actionSave = UIAlertAction(title: alertTitleSave, style: UIAlertActionStyle.default) { (SAVE : UIAlertAction) in
            
            //get all text from fields
            handlerSave()
        }
        
        //add input
        var textPlaceHolders : [UITextField] = []
        var index : Int = 0
        
        for placeholder in inputPlaceHolders {
            
            alertController.addTextField { (textField : UITextField) in
                
                if placeholder == kFieldPassword || placeholder == kFieldConfirmPassword {
                    textField.isSecureTextEntry = true
                } else {
                    textField.isSecureTextEntry = false
                }
                textField.placeholder = placeholder
                textField.delegate = target as? UITextFieldDelegate
                objc_setAssociatedObject(textField, &keyAssociatedAactionSave, actionSave, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
                textField.tag = index
                textPlaceHolders.append(textField)
                index = index + 1
            }
        }
        objc_setAssociatedObject(alertController, &keyAssociatedAlertPlaceHolder, textPlaceHolders, objc_AssociationPolicy.OBJC_ASSOCIATION_COPY_NONATOMIC)
        
        alertController .addAction(actionCancel)
        
        actionSave.isEnabled = false
        alertController .addAction(actionSave)
        
        target.present(alertController, animated: animated, completion: nil)
    }
    
    class func showAddToCartAlert(target : UIViewController, animated : Bool, message : String, handlerCancel:@escaping (()->Void), handlerOk:@escaping (()->Void)) {
        
        let alertController = UIAlertController(title: "message".localized, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        let actionCancel = UIAlertAction(title: "Continue Shopping", style: UIAlertActionStyle.default) { (CANCEL : UIAlertAction) in
            
            handlerCancel()
        }
        
        let actionSave = UIAlertAction(title: "Go To Cart", style: UIAlertActionStyle.default) { (SAVE : UIAlertAction) in
            
            handlerOk()
        }
        
        alertController .addAction(actionCancel)
        alertController .addAction(actionSave)
        
        target.present(alertController, animated: animated, completion: nil)
    }
    
    //MARK: Display Alert With Cancel Ok Button
    class func showAlertWithCancelOk(target : UIViewController, animated : Bool, message : String, handlerCancel:(()->Void)? = nil, handlerOk:@escaping (()->Void)) {
        
        let alertController = UIAlertController(title: "message".localized, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        let actionCancel = UIAlertAction(title: alertTitleCancel, style: UIAlertActionStyle.default) { (CANCEL : UIAlertAction) in
            
            handlerCancel?()
        }
        
        let actionSave = UIAlertAction(title: alertTitleOk, style: UIAlertActionStyle.default) { (SAVE : UIAlertAction) in
            
            handlerOk()
        }
        
        alertController .addAction(actionCancel)
        alertController .addAction(actionSave)
        
        target.present(alertController, animated: animated, completion: nil)
    }
    
    //MARK: Display Alert With Yes No Button
    func showAlertWithNoYes(target : UIViewController, animated : Bool, message : String, handlerNo:(()->Void)? = nil, _ handlerYes:@escaping (()->Void)) {
        
        let alertController = UIAlertController(title: "message".localized, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        let actionCancel = UIAlertAction(title: alertTitleNo, style: UIAlertActionStyle.default) { (CANCEL : UIAlertAction) in
            
            handlerNo?()
        }
        
        let actionSave = UIAlertAction(title: alertTitleYes, style: UIAlertActionStyle.default) { (SAVE : UIAlertAction) in
            
            handlerYes()
        }
        
        alertController .addAction(actionCancel)
        alertController .addAction(actionSave)
        
        target.present(alertController, animated: animated, completion: nil)
    }
}
