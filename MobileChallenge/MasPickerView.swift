//
//  MasPickerView.swift
//  Thoag
//
//  Created by Masum Ahmed on 9/12/19.
//  Copyright © 2019 Ashraf Ul Alam Tusher. All rights reserved.
//

import UIKit

//MARK: - TypeAlias For CallBack
typealias ItemSelectedCallback<inputType> = ((inputType?) -> Void)

class MasPickerView: UIPickerView {
    
    //MARK:- Varibale Declaration
    var arrayPickerData = [MasPickerData]()
    var selectedItemCallback: ItemSelectedCallback<MasPickerData>?
    
    var defaultSelected: MasPickerData? {
        didSet {
            guard defaultSelected != nil else {
                return
            }
            
            if let index = arrayPickerData.index(where: { $0.value.lowercased() == defaultSelected!.value.lowercased()}) {
                self.selectRow(index, inComponent: 0, animated: false)
                selectedItemCallback!(arrayPickerData[index])
            }
        }
    }
    
    //MARK:- Class Life Cycle Methods
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        guard arrayPickerData.count > 0 else { return }
        
        if defaultSelected != nil {
            if let index = arrayPickerData.index(where: { $0.value.lowercased() == defaultSelected!.value.lowercased()}) {
                self.selectRow(index, inComponent: 0, animated: false)
                selectedItemCallback!(arrayPickerData[index])
            }else{
                selectedItemCallback!(arrayPickerData[0])
            }
        }else{
            selectedItemCallback!(arrayPickerData[0])
        }
        // Drawing code
    }
    
    //MARK:- init() Method
    init(_ arrayPickerData: [MasPickerData], defaultSelected: MasPickerData?, selectedItemCallback: ItemSelectedCallback<MasPickerData>?) {
        
        super.init(frame: CGRect.zero)
        
        self.delegate = self
        self.dataSource = self
        
        self.arrayPickerData = arrayPickerData
        self.defaultSelected = defaultSelected
        self.selectedItemCallback = selectedItemCallback
        
        self.reloadAllComponents()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//MARK:- Picker View DataSource Mehtods
extension MasPickerView: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrayPickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrayPickerData[row].value
    }
}

//MARK:- Picker View Delegate Mehtods
extension MasPickerView: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        //return callback of selected data
        guard arrayPickerData.count > 0 else {
            return
        }

        selectedItemCallback!(arrayPickerData[row])
    }
}


struct MasPickerData {
    var id: String
    var value: String
}
