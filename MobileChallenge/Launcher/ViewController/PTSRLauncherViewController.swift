//
//  ViewController.swift
//  sample-run
//
//  Created by PayTabs LLC on 10/5/17.
//  Copyright © 2017 PayTabs LLC. All rights reserved.
//

import UIKit

class PTSRLauncherViewController: UIViewController {
//    // MARK: Outlets
//    @IBOutlet weak var launcherView: PTSRLauncherView!
//    
//    // MARK: Instance Variables
//    var initialSetupViewController: PTFWInitialSetupViewController!
//    
//    // MARK: View Lifecycle
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        
//        self.configureCallbacks()
//        handleHitTestRunTapEvent()
//        
//    }
//    
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(true)
//    }
//    
//    override func viewWillDisappear(_ animated: Bool) {
//        super.viewWillDisappear(true)
//    }
//    
//    override func viewDidAppear(_ animated: Bool) {
//        super.viewDidAppear(true)
//    }
//    
//    override func viewDidDisappear(_ animated: Bool) {
//        super.viewDidDisappear(true)
//    }
//    
//    // MARK: Private Methods
//    // MARK: Objects
//    private func initiateSDK() {
//        self.launcherView.endEditing(true)
//        let bundle = Bundle(url: Bundle.main.url(forResource: ApplicationResources.kFrameworkResourcesBundle, withExtension: "bundle")!)
//        
//        self.initialSetupViewController = PTFWInitialSetupViewController.init(
//            nibName: ApplicationXIBs.kPTFWInitialSetupView,
//            bundle: bundle,
//            andWithViewFrame: self.view.frame,
//
//            andWithAmount: Float(grandFinalTotal),
//            andWithCustomerTitle: "PayTabs",
//            andWithCurrencyCode: " SAR",
//            andWithTaxAmount: 0.0,
//            andWithSDKLanguage: "en",
//            andWithShippingAddress: "jeddah KSA",
//            andWithShippingCity: "jeddah",
//            andWithShippingCountry: "BHR",
//            andWithShippingState: "123",
//            andWithShippingZIPCode: "097",
//            andWithBillingAddress: "Manama Bahrain",
//            andWithBillingCity: "Manama",
//            andWithBillingCountry: "BHR",
//            andWithBillingState: "Manama",
//            andWithBillingZIPCode: "0097",
//            andWithOrderID: orderNumber,
//            andWithPhoneNumber: (UserDefaults.standard.value(forKey: "userPhone") as! String?)!,
//            andWithCustomerEmail: (UserDefaults.standard.value(forKey: "userEmail") as! String?)!,
//            andWithCustomerPassword: "rVoPaGMCaL",
//            andIsTokenization: false,
//            andIsExistingCustomer: false,
//            andWithPayTabsToken: "s7oeTQxwJGpNGjFymzOSLWafapeIzBG1",
//            andWithMerchantEmail: "tusher.pisqr@gmail.com",
//            andWithMerchantSecretKey: "2QrVmY5AGpyuFp8u1NlgSJ8HK1Lbtcuv2VroTwZAWy5CXd6Y0rfSbnAWpl8SyCnAyPZsmZ2ix7Vuk2tltIeF5OCuaLmInKjVAjoW",
//            andWithRequestTimeoutSeconds: 200,
//            andWithAssigneeCode: "SDK")
//        // 2QrVmY5AGpyuFp8u1NlgSJ8HK1Lbtcuv2VroTwZAWy5CXd6Y0rfSbnAWpl8SyCnAyPZsmZ2ix7Vuk2tltIeF5OCuaLmInKjVAjoW
//        // i81eqeFXBSIr9xEp90VOde2s9CSUXVff3gutu9q619ybGcBQcwFqLHTUkMOhH4xKk2dPbEVdNupuG1lTy0kjIed202jI6RtqTHg5
//        weak var weakSelf = self
//        self.initialSetupViewController.didReceiveBackButtonCallback = {
//            weakSelf?.handleBackButtonTapEvent()
//        }
//        
//        self.initialSetupViewController.didReceiveFinishTransactionCallback = {(responseCode, result, transactionID, tokenizedCustomerEmail, tokenizedCustomerPassword, token, transactionState) in
////            self.launcherView.responseCodeLabel.text = "\(responseCode)"
////            self.launcherView.resultLabel.text = "\(result)"
////            self.launcherView.transactionIDLabel.text = "\(transactionID)"
////            self.launcherView.customerEmailLabel.text = "\(tokenizedCustomerEmail)"
////            self.launcherView.customerPasswordLabel.text = "\(tokenizedCustomerPassword)"
////            self.launcherView.transactionStateLabel.text = "\(transactionState)"
////            self.launcherView.tokenValueLabel.text = "\(token)"
////
////            self.launcherView.responseView.isHidden = false
//            self.navigationController?.navigationBar.isHidden = false
//            comingFromCard = ""
//            couponValue = ""
//            couponUcode = ""
//            cartDetails.removeAll()
//            cartObject.cartArr.removeAll()
//            let vc = UIStoryboard(name: "Main", bundle:nil).instantiateViewController(withIdentifier: "OrdersuccessfulVC") as! OrdersuccessfulVC
//            self.navigationController?.pushViewController(vc, animated: true)
//            
//            weakSelf?.handleBackButtonTapEvent()
//        }
//    }
//    
//    // MARK: Callbacks
//    private func configureCallbacks() -> Void {
//        weak var weakSelf = self
//        
//        self.launcherView.didPressHitTestRunButtonCallback = {
//            weakSelf?.handleHitTestRunTapEvent()
//        }
//        
//        self.launcherView.didPressResponseViewCloseButtonCallback = {
//            weakSelf?.handleCloseResponseViewTapEvent()
//        }
//    }
//    
//    // MARK: Event Methods
//    // MARK: Initiate SDK Event
//    private func handleHitTestRunTapEvent() {
//            self.initiateSDK()
//            self.view.addSubview(initialSetupViewController.view)
//            self.addChildViewController(initialSetupViewController)
//            initialSetupViewController.didMove(toParentViewController: self)
//    }
//    
//    // MARK: Close SDK Event
//    private func handleBackButtonTapEvent() {
//        self.initialSetupViewController.willMove(toParentViewController: self)
//        self.initialSetupViewController.view.removeFromSuperview()
//        self.initialSetupViewController.removeFromParentViewController()
//    }
//    
//    // MARK: Close Response window Event
//    private func handleCloseResponseViewTapEvent() {
//        navigationController?.navigationBar.isHidden = false
//        navigationController?.popViewController(animated: true)
//    }
//    
    
}

