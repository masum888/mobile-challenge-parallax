//
//  View.swift
//  Expo Mobile
//
//  Created by Onebyte LLC on 6/9/17.
//  Copyright © 2017 Onebyte LLC. All rights reserved.
//

import Foundation
import UIKit

class PTSRLauncherView: UIView {
    // MARK: Outlets
    

    
    @IBOutlet weak var amountTextField: UITextField!
    @IBOutlet weak var customerEmailTextField: UITextField!
    @IBOutlet weak var currencyTextField: UITextField!

    @IBOutlet weak var responseCodeLabel: UILabel!
    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet weak var transactionIDLabel: UILabel!
    @IBOutlet weak var customerEmailLabel: UILabel!
    @IBOutlet weak var customerPasswordLabel: UILabel!
    @IBOutlet weak var transactionStateLabel: UILabel!
    @IBOutlet weak var tokenValueLabel: UILabel!

    @IBOutlet weak var tokenizationSwitch: UISwitch!

    @IBOutlet weak var responseView: UIView!

    // MARK: Callbacks
    public var didPressHitTestRunButtonCallback : (() -> Void)?
    public var didPressResponseViewCloseButtonCallback : (() -> Void)?

    // MARK: Overriden methods
    override func awakeFromNib() {
        self.configureView()
    }
    
    // MARK: Private methods
    private func configureView() {
        self.handleHitTestRunButtonTapEvent()
    }
    
    // MARK: Action Methods
    @IBAction func hitTestRunButtonPressed(_ sender: AnyObject) {
        self.handleHitTestRunButtonTapEvent()
    }

    @IBAction func closeResponseViewRunButtonPressed(_ sender: AnyObject) {
        self.handleCloseResponseViewButtonTapEvent()
    }
    
    // MARK: Events
    private func handleHitTestRunButtonTapEvent() {
        if didPressHitTestRunButtonCallback != nil {
            didPressHitTestRunButtonCallback!()
        }
    }
    
    private func handleCloseResponseViewButtonTapEvent() {
        if didPressResponseViewCloseButtonCallback != nil {
            didPressResponseViewCloseButtonCallback!()
        }
    }
}
