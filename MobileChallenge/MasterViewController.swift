//
//  MasterViewController.swift
//  Thoag
//
//  Created by Tusher on 3/12/19.
//  Copyright © 2019 Ashraf Ul Alam Tusher. All rights reserved.
//

import UIKit
import SwiftEntryKit
//import WeScan
import NotificationCenter


// for checking access add below
import CoreLocation
import Photos
import AVFoundation

//


class MasterViewController: UIViewController {


    override func viewDidLoad() {
        super.viewDidLoad()
        

        // Do any additional setup after loading the view.
    }

    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    
    @objc func languageDidChange(notification: Notification){
        languageDidChange()
    }
    
    func languageDidChange()
    {
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        

        

    }


    
    
    func showAlertView(attributes: EKAttributes) {
        
        // Generate textual content
        let title = EKProperty.LabelContent(text: "ভাষা নির্বাচন করুন", style: .init(font: MainFont.medium.with(size: 15), color: .black, alignment: .center))
        let description = EKProperty.LabelContent(text: "বাংলাদেশ পররাষ্ট্র মন্ত্রণালয় অ্যাপ দূতাবাস এ আপনাকে স্বাগতম, দয়া করে ভাষা নির্বাচন করুন।", style: .init(font: MainFont.light.with(size: 13), color: .black, alignment: .center))
        let image = EKProperty.ImageContent(imageName: "thoag_icon_black", size: CGSize(width: 70, height: 70), contentMode: .scaleAspectFit)
        let simpleMessage = EKSimpleMessage(image: image, title: title, description: description)
        
        // Generate buttons content
        let buttonFont = MainFont.medium.with(size: 16)
        
        // Close button
        let closeButtonLabelStyle = EKProperty.LabelStyle(font: buttonFont, color: EKColor.Gray.a800)
        let closeButtonLabel = EKProperty.LabelContent(text: UserDefaults.standard.value(forKey: "lang") as! String == "1" ? "Cancel" : "বাতিল করুন", style: closeButtonLabelStyle)
        let closeButton = EKProperty.ButtonContent(label: closeButtonLabel, backgroundColor: .clear, highlightedBackgroundColor:  EKColor.Gray.a800.withAlphaComponent(0.05)) {
            SwiftEntryKit.dismiss()
            // set Language
        }
        
        // Remind me later Button
        let laterButtonLabelStyle = EKProperty.LabelStyle(font: buttonFont, color: EKColor.Teal.a600)
        let laterButtonLabel = EKProperty.LabelContent(text: "English", style: laterButtonLabelStyle)
        let laterButton = EKProperty.ButtonContent(label: laterButtonLabel, backgroundColor: .clear, highlightedBackgroundColor:  EKColor.Teal.a600.withAlphaComponent(0.05)) {
            SwiftEntryKit.dismiss()
            
            // set language
            UserDefaults.standard.set("1", forKey: "lang")
            var transition: UIViewAnimationOptions = .transitionFlipFromLeft
            if L102Language.currentAppleLanguage() == "bn" {
                L102Language.setAppleLAnguageTo(lang: "en")
            }else {
                L102Language.setAppleLAnguageTo(lang: "en")
                transition = .transitionFlipFromRight
            }
            
            let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
            rootviewcontroller.rootViewController = UIStoryboard(name: "Main", bundle:nil).instantiateViewController(withIdentifier: "initialVC")
            let mainwindow = (UIApplication.shared.delegate?.window!)!
            mainwindow.backgroundColor = UIColor(hue: 0.6477, saturation: 0.6314, brightness: 0.6077, alpha: 0.8)
            UIView.transition(with: mainwindow, duration: 0.55001, options: transition, animations: { () -> Void in
            }) { (finished) -> Void in
                
            }
            
        }
        
        // Ok Button
        let okButtonLabelStyle = EKProperty.LabelStyle(font: buttonFont, color: EKColor.Teal.a600)
        let okButtonLabel = EKProperty.LabelContent(text: "বাংলা", style: okButtonLabelStyle)
        let okButton = EKProperty.ButtonContent(label: okButtonLabel, backgroundColor: .clear, highlightedBackgroundColor:  EKColor.Teal.a600.withAlphaComponent(0.05)) {
            SwiftEntryKit.dismiss()
            
            // set Language
            UserDefaults.standard.set("2", forKey: "lang")
            var transition: UIViewAnimationOptions = .transitionFlipFromLeft
            if L102Language.currentAppleLanguage() == "en" {
                L102Language.setAppleLAnguageTo(lang: "bn")
            } else {
                L102Language.setAppleLAnguageTo(lang: "bn")
                transition = .transitionFlipFromRight
            }
            
            let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
            rootviewcontroller.rootViewController = UIStoryboard(name: "Main", bundle:nil).instantiateViewController(withIdentifier: "initialVC")
            let mainwindow = (UIApplication.shared.delegate?.window!)!
            mainwindow.backgroundColor = UIColor(hue: 0.6477, saturation: 0.6314, brightness: 0.6077, alpha: 0.8)
            UIView.transition(with: mainwindow, duration: 0.55001, options: transition, animations: { () -> Void in
            }) { (finished) -> Void in
                
            }
            
        }
        
        // Generate the content
        let buttonsBarContent = EKProperty.ButtonBarContent(with: okButton, laterButton, closeButton, separatorColor: EKColor.Gray.light, expandAnimatedly: true)
        let alertMessage = EKAlertMessage(simpleMessage: simpleMessage, buttonBarContent: buttonsBarContent)
        // Setup the view itself
        let contentView = EKAlertMessageView(with: alertMessage)
        SwiftEntryKit.display(entry: contentView, using: attributes)
    }
    
    
    // MARK: Props
    let dataSource = PresetsDataSource()
    
    func showNotificationMessage(attributes: EKAttributes, title: String, desc: String, textColor: UIColor, imageName: String) {
        let title = EKProperty.LabelContent(text: title, style: .init(font: MainFont.medium.with(size: 16), color: textColor))
        let description = EKProperty.LabelContent(text: desc, style: .init(font: MainFont.light.with(size: 14), color: textColor))
        let image = EKProperty.ImageContent(image: UIImage(named: imageName)!, size: CGSize(width: 40, height: 35))
        let simpleMessage = EKSimpleMessage(image: image, title: title, description: description)
        let notificationMessage = EKNotificationMessage(simpleMessage: simpleMessage)
        
        let contentView = EKNotificationMessageView(with: notificationMessage)
        SwiftEntryKit.display(entry: contentView, using: attributes)
    }
    
    func showPopupMessage(attributes: EKAttributes, title: String, titleColor: UIColor, description: String, descriptionColor: UIColor, buttonTitleColor: UIColor, buttonBackgroundColor: UIColor, image: UIImage, imagePosition: EKPopUpMessage.ImagePosition = .topToTop(offset: 40)) {
        let title = EKProperty.LabelContent(text: title, style: .init(font: MainFont.medium.with(size: 24), color: titleColor, alignment: .center))
        let description = EKProperty.LabelContent(text: description, style: .init(font: MainFont.light.with(size: 16), color: descriptionColor, alignment: .center))
        let button = EKProperty.ButtonContent(label: .init(text: "Got it!", style: .init(font: MainFont.bold.with(size: 16), color: buttonTitleColor)), backgroundColor: buttonBackgroundColor, highlightedBackgroundColor: buttonTitleColor.withAlphaComponent(0.05))
        let topImage = EKProperty.ImageContent(image: image, size: CGSize(width: 60, height: 60), contentMode: .scaleAspectFit)
        let message = EKPopUpMessage(topImage: topImage, imagePosition: imagePosition, title: title, description: description, button: button) {
            SwiftEntryKit.dismiss()
        }
        
        let contentView = EKPopUpMessageView(with: message)
        SwiftEntryKit.display(entry: contentView, using: attributes)
    }
    
    func showAlert (errorMessage: String) -> Void{
        let alertController = UIAlertController(title: "message".localized, message: errorMessage, preferredStyle: .alert)
        let action2 = UIAlertAction(title: "try_again_please".localized, style: .cancel) { (action) in
            print("Cancel is pressed......")
        }
        alertController.addAction(action2)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showAlertSuccess (errorMessage: String) -> Void{
        let alertController = UIAlertController(title: UserDefaults.standard.value(forKey: "lang") as! String == "1" ? "Thank You" : "ধন্যবাদ", message: errorMessage, preferredStyle: .alert)
        let action2 = UIAlertAction(title: UserDefaults.standard.value(forKey: "lang") as! String == "1" ? "Continue" : "পরবর্তী ধাপ", style: .cancel) { (action) in
            print("Cancel is pressed......")
        }
        alertController.addAction(action2)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showAlertMaster(title: String, errorMessage: String) -> Void{
        let alertController = UIAlertController(title: title, message: errorMessage, preferredStyle: .alert)
        let action2 = UIAlertAction(title: UserDefaults.standard.value(forKey: "lang") as! String == "1" ? "Continue" : "চালিয়ে যান", style: .cancel) { (action) in
            print("Cancel is pressed......")
        }
        alertController.addAction(action2)
        self.present(alertController, animated: true, completion: nil)
    }

    
    
    private func showProcessingNote(attributes: EKAttributes) {
        let text = "please_wait".localized
        let style = EKProperty.LabelStyle(font: MainFont.light.with(size: 14), color: .white, alignment: .center)
        let labelContent = EKProperty.LabelContent(text: text, style: style)
        let contentView = EKProcessingNoteMessageView(with: labelContent, activityIndicator: .white)
        SwiftEntryKit.display(entry: contentView, using: attributes)
    }
    
    

}
