//
//  MCProductListVC.swift
//  MobileChallenge
//
//  Created by Masum Ahmed on 16/8/21.
//  Copyright © 2021 Ashraf Ul Alam Tusher. All rights reserved.
//

import UIKit
import SwiftEntryKit

class MCProductListVC: MasterViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var storeName: UILabel!
    @IBOutlet weak var rating: UILabel!
    @IBOutlet weak var openingTime: UILabel!
    @IBOutlet weak var closingTime: UILabel!
    @IBOutlet weak var productListTV: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        productListTV.dataSource = self
        productListTV.delegate = self

        loadPageData()
    }
    
    
    func loadPageData()
    {
        let progressHUD = ProgressHUD(text: "Loading...")
        self.view.addSubview(progressHUD)
        progressHUD.show()
        
        mcObject.getStoreInfo(success: { (status, message) in
            
            print(status)
            print(message)

            
            if status == 200 {
                
                self.storeName.text = storeInfo.name
                self.rating.text = String(storeInfo.rating ?? 0)
                self.openingTime.text = storeInfo.openingTime
                self.closingTime.text = storeInfo.closingTime
                
                mcObject.getProductInfo(success: { (status, message) in
                    
                    print(status)
                    print(message)
                    
                    progressHUD.hide()
                    
                    if status == 200 {

                        self.productListTV.reloadData()

                    } else if status == 100{

                        MasAlertViewManager.showAlert(target: self, animated: true, message: message)
                        
                    }
                    
                }) { (message) in

                    progressHUD.hide()
                    // Set note label content
                    let text = message
                    let style = EKProperty.LabelStyle(font: MainFont.light.with(size: 14), color: .white, alignment: .center)
                    let labelContent = EKProperty.LabelContent(text: text, style: style)
                    let imageContent = EKProperty.ImageContent(image: UIImage(named: "ic_wifi")!)
                    let contentView = EKImageNoteMessageView(with: labelContent, imageContent: imageContent)
                    SwiftEntryKit.display(entry: contentView, using: self.dataSource[1, 2].attributes)
                    
                }
                

            } else if status == 100{

                MasAlertViewManager.showAlert(target: self, animated: true, message: message)
                
            }
            
        }) { (message) in

            progressHUD.hide()
            // Set note label content
            let text = message
            let style = EKProperty.LabelStyle(font: MainFont.light.with(size: 14), color: .white, alignment: .center)
            let labelContent = EKProperty.LabelContent(text: text, style: style)
            let imageContent = EKProperty.ImageContent(image: UIImage(named: "ic_wifi")!)
            let contentView = EKImageNoteMessageView(with: labelContent, imageContent: imageContent)
            SwiftEntryKit.display(entry: contentView, using: self.dataSource[1, 2].attributes)
            
        }
    }
    

    @IBAction func continueBtnPressed(_ sender: Any) {
        let vc = UIStoryboard(name: "Main", bundle:nil).instantiateViewController(withIdentifier: "MCConfirmOrderVC") as! MCConfirmOrderVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    
    //MARK:- TableView
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return productList.list?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MCProductListTVCell", for: indexPath) as! MCProductListTVCell
        cell.index = indexPath.row
        cell.updateCellWith(row: productList.list?[indexPath.row] ?? MCProduct())
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {

    }

}

class MCProductListTVCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    var rowProductData: MCProduct = MCProduct()
    var index = 0
    
    @IBOutlet weak var cv: UICollectionView!
    @IBOutlet weak var cvHeightCons: NSLayoutConstraint!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical //.horizontal
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.itemSize = CGSize(width: (UIScreen.main.bounds.size.width - 80), height: 110)
        self.cv.setCollectionViewLayout(layout, animated: true)
        
        self.cv.isPagingEnabled = false
        self.cv.dataSource = self
        self.cv.delegate = self
        //self.cv.tag = row
        self.cv.contentInset.left = 0
        //self.cv.reloadData()
        


    }
    
    func updateCellWith(row: MCProduct) {
        self.rowProductData = row
        self.cv.reloadData()
        let rcount = 1
        let a = rcount * 110
        cvHeightCons.constant = CGFloat(a)
        self.layoutIfNeeded()
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = cv.dequeueReusableCell(withReuseIdentifier: "MCProductRowCVCell", for: indexPath) as? MCProductRowCVCell {


            cell.itemIndex = indexPath.item
            cell.pname.text = rowProductData.name

            
            cell.pimg.setImage(imageURL: rowProductData.imageUrl ?? "", placeholderImage: UIImage(systemName: "photo")!)

            cell.qtyLbl.text = "\(rowProductData.qty ?? 1)"
            
            let dqty = Double((rowProductData.qty ?? 1) )
            let dprice = Double((rowProductData.price ?? 0) )
            let totalp = dqty * dprice
            
            cell.total.text = "\( totalp )"


            //if productList.list?[indexPath.row].selected == false {
            if selectedProductList.filter({ $0.name == self.rowProductData.name }).count == 0 {
                cell.itemChkImg.image = UIImage(systemName: "square")
                cell.itemChkImg.tintColor = #colorLiteral(red: 0.6823529412, green: 0.6823529412, blue: 0.6980392157, alpha: 1)
            }else if selectedProductList.filter({ $0.name == self.rowProductData.name }).count > 0 {
            //}else if productList.list?[indexPath.row].selected == true {
                cell.itemChkImg.image = UIImage(systemName: "checkmark.square.fill")
                cell.itemChkImg.tintColor = #colorLiteral(red: 0.2804988325, green: 0.3969599605, blue: 0.5673219562, alpha: 1)
            }
            
            cell.qtyplusbtntapped = {(cell) in
                print("qtyplusbtntapped")
                
                
                if ((self.parentContainerViewController() as? MCProductListVC) != nil) {
                
                    if let cell = cell as? MCProductRowCVCell {
                        
                        let max = 100
                        
                        let b = Int( self.rowProductData.qty ?? 0 )
                        
                        if b < max {
                            let n = b + 1
                            cell.qtyLbl.text = String(n)
                            print("QTY PLUS", n)
                            
                            //self.rowProductData.qty = n
                            productList.list?[self.index].qty = n
                            
                            let dqty = Double((self.rowProductData.qty ?? 1) )
                            let dprice = Double((self.rowProductData.price ?? 0) )
                            let totalp = dqty * dprice
                            
                            cell.total.text = "\( totalp )"
                        }
                    }
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "selectedItemChanged"), object: nil)
                    
                }
            }
            
            cell.qtyminusbtntapped = {(cell) in
                print("qtyminusbtntapped")
                
                if ((self.parentContainerViewController() as? MCProductListVC) != nil) {
                
                    if let cell = cell as? MCProductRowCVCell {
                        
                        let min = 1
                        
                        let b = Int( self.rowProductData.qty ?? 0 )
                        print("qtyminusbtntapped b",b)
                        
                        if b > min {
                            let n = b - 1
                            cell.qtyLbl.text = String(n)
                            print("QTY MINUS", n)
                            
                            //self.rowProductData.qty = n
                            productList.list?[self.index].qty = n
                            
                            let dqty = Double((self.rowProductData.qty ?? 1) )
                            let dprice = Double((self.rowProductData.price ?? 0) )
                            let totalp = dqty * dprice
                            
                            cell.total.text = "\( totalp )"

                        }
                    }
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "selectedItemChanged"), object: nil)
                    
                }
            }
            
            cell.selectitembtntapped = { (cell) in
                print("selectitembtntapped")
                
                if ((self.parentContainerViewController() as? MCProductListVC) != nil) {
                    
                    
                    if let cell = cell as? MCProductRowCVCell {

                        if selectedProductList.filter({ $0.name == self.rowProductData.name }).count > 0 {

                            
                            cell.itemChkImg.image = UIImage(systemName: "square")
                            cell.itemChkImg.tintColor = #colorLiteral(red: 0.6823529412, green: 0.6823529412, blue: 0.6980392157, alpha: 1)
                            
                            selectedProductList = selectedProductList.filter({ $0.name != self.rowProductData.name })

                        }else if selectedProductList.filter({ $0.name == self.rowProductData.name }).count == 0 {

                            selectedProductList.append(self.rowProductData)


                            cell.itemChkImg.image = UIImage(systemName: "checkmark.square.fill")
                            cell.itemChkImg.tintColor = #colorLiteral(red: 0.2804988325, green: 0.3969599605, blue: 0.5673219562, alpha: 1)

                            
                        }
                        
                        NotificationCenter.default.post(name: Notification.Name(rawValue: "selectedItemChanged"), object: nil)
                    }
                    
                    
                }
                
                
                


            }
            


            return cell
        }
        return UICollectionViewCell()
    }
    


    
}


class MCProductRowCVCell : UICollectionViewCell {

    var itemIndex = 0
    @IBOutlet weak var itemChkImg: UIImageView!
    var selectitembtntapped: ((UICollectionViewCell) -> Void)?
    var qtyplusbtntapped: ((UICollectionViewCell) -> Void)?
    var qtyminusbtntapped: ((UICollectionViewCell) -> Void)?
    @IBAction func selectitembtntapped(_ sender: Any) {
        selectitembtntapped?(self)
    }
    @IBAction func qtyplusbtntapped(_ sender: Any) {
        qtyplusbtntapped?(self)
    }
    @IBAction func qtyminusbtntapped(_ sender: Any) {
        qtyminusbtntapped?(self)
    }
    @IBOutlet weak var pimg: UIImageView!
    @IBOutlet weak var pname: UILabel!
    @IBOutlet weak var total: UILabel!
    @IBOutlet weak var qtyLbl: UILabel!
}
