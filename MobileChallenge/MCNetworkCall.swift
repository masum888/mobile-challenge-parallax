//
//  MCNetworkCall.swift
//  MobileChallenge
//
//  Created by Masum Ahmed on 16/8/21.
//  Copyright © 2021 Ashraf Ul Alam Tusher. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON



class MCNetworkCalls {
    
    
    func getStoreInfo(success: @escaping(_ status: Int, _ message: String)-> Void, failure: @escaping(_ message: String)-> Void){
        
        let url = Base_Url + "storeInfo"
        
        let Headers : [String:String] = [
            "accept":"application/json"
        ]

        
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: Headers)
            .validate()
            .responseJSON { (response) in
                
                switch response.result {
                case.success(let result):
                    
                    let jsonResult = JSON(result)
                    print("getStoreInfo json",jsonResult)
                    
                    let statusCode = response.response?.statusCode
                    
                    if statusCode == 200 {

                        storeInfo = MCStoreInfo(fromJSON: jsonResult)
                        success(200, "Success")
                        
                    }else{

                        success(100, "Error")
                        
                    }
                    
                    
                    
                case.failure(let error):
                    if let err = response.result.error as NSError? {
                        if (err.code == -1009){
                            print(err.code)
                            let errTypInternet = no_internet_connection
                            failure(errTypInternet)
                        }else{
                            let errTypNetwork = network_connection_error
                            failure(errTypNetwork)
                        }
                    }
                    print("error: \(error)")
                }
        }
        
    }
    
    
    func getProductInfo(success: @escaping(_ status: Int, _ message: String)-> Void, failure: @escaping(_ message: String)-> Void){
        
        let url = Base_Url + "products"
        
        let Headers : [String:String] = [
            "accept":"application/json"
        ]

        
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: Headers)
            .validate()
            .responseJSON { (response) in
                
                switch response.result {
                case.success(let result):
                    
                    let jsonResult = JSON(result)
                    print("getProductInfo json",jsonResult)
                    
                    let statusCode = response.response?.statusCode

                    if statusCode == 200 {

                        productList = MCProductList(fromJSON: jsonResult)
                        success(200, "Success")
                        
                    }else{

                        success(100, "Error")
                        
                    }
                    
                case.failure(let error):
                    if let err = response.result.error as NSError? {
                        if (err.code == -1009){
                            print(err.code)
                            let errTypInternet = no_internet_connection
                            failure(errTypInternet)
                        }else{
                            let errTypNetwork = network_connection_error
                            failure(errTypNetwork)
                        }
                    }
                    print("error: \(error)")
                }
        }
        
    }
    
    
    func postOrder(delivery_address: String, products: [MCProduct], success: @escaping(_ status: Int, _ message: String)-> Void, failure: @escaping(_ message: String)-> Void){

        
                let urlstring = "https://virtserver.swaggerhub.com/m-tul/opn-mobile-challenge-api/1.0.0/order"
                let parameters: [String: Any] =
                    [
                        "delivery_address": delivery_address,
                        "products": products.map({$0.toDictionary()})
                    
                    ]

                Alamofire.request(urlstring, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON { response in
                    
                    let statusCode = response.response?.statusCode
                    
                    
                    if statusCode == 201 {

                        success(200, "Success")

                    }else{

                        success(100, "Error")

                    }
                    
                }
        
        
        
        
        
        
        
        
        
        
    }
    
    
    
}
