//
//  RegisterVC.swift
//  Thoag
//
//  Created by Tusher on 2/26/19.
//  Copyright © 2019 Ashraf Ul Alam Tusher. All rights reserved.
//

import UIKit
import SwiftEntryKit
import WeScan
import NotificationCenter

class ChangeMissionVC: MasterViewController, ImageScannerControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    
    
    //variables
    var passportDocImage: UIImage!
    var nidDocImage: UIImage!
    var tappFlag = 0
    var pickedDate = ""
    var pickedConsulate = ""
    var pickedConsulateID = ""
    
    //connections
    @IBOutlet weak var progressBlurView: UIView!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var nameTxtLbl: UITextField!
    @IBOutlet weak var nidTxtLbl: UITextField!
    @IBOutlet weak var passportTxtLbl: UITextField!
    @IBOutlet weak var passportExpDateTxtLbl: UITextField!
    @IBOutlet weak var emailAddressTxtLbl: UITextField!
    @IBOutlet weak var phoneNumber: UITextField!
    @IBOutlet weak var passwordTxtLbl: UITextField!
    @IBOutlet weak var passportScannedImage: UIImageView!
    @IBOutlet weak var nidScannedImage: UIImageView!
    @IBOutlet weak var datePickerView: UIView!
    @IBOutlet weak var paddressL1: UITextField!
    @IBOutlet weak var consularPicker: UIPickerView!
    @IBOutlet weak var successView: UIView!
    @IBOutlet weak var selectConsulateView: UIView!
    @IBOutlet weak var missionNameTxt: UITextField!
    @IBOutlet weak var selectMissionBtn: UIButton!
    
    // label from translation
    @IBOutlet weak var pageTitle: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var nidLabel: UILabel!
    @IBOutlet weak var passportLabel: UILabel!
    @IBOutlet weak var passportDateExLable: UILabel!
    @IBOutlet weak var emailAddressLabel: UILabel!
    @IBOutlet weak var selectPasswordLabel: UILabel!
    @IBOutlet weak var mobileNumberLabel: UILabel!
    @IBOutlet weak var serviceDeliveryStation: UILabel!
    @IBOutlet weak var currentAddress: UILabel!
    @IBOutlet weak var addressLineLabel: UILabel!
    @IBOutlet weak var passposrtNidTitleLabel: UILabel!
    @IBOutlet weak var passportScanLable: UILabel!
    @IBOutlet weak var nidScanLable: UILabel!
    @IBOutlet weak var completeRegistrationBtn: UIButton!
    @IBOutlet weak var selectDateBtnLable: UIButton!
    @IBOutlet weak var cancelDateBtnLable: UIButton!
    @IBOutlet weak var uploadProgressLable: UILabel!
    @IBOutlet weak var successMessageTitle: UILabel!
    @IBOutlet weak var successMessageBody: UILabel!
    @IBOutlet weak var startUsingAppBtnLable: UIButton!
    @IBOutlet weak var cancelConsulateSelectionBtnLable: UIButton!
    @IBOutlet weak var selectConsulateSelectionBtnLable: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        datePickerView.isHidden = true
        progressBlurView.isHidden = true
        successView.isHidden = true
        selectConsulateView.isHidden = true
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.uploadProgressBar(notification:)), name: NSNotification.Name(rawValue: "uploadProgress"), object: nil)
        if userObject.missiontempArr.count == 0{
            selectMissionBtn.isEnabled = false
        }else{
            selectMissionBtn.isEnabled = true
        }
        
        // translation
        pageTitle.text = UserDefaults.standard.value(forKey: "lang") as! String == "1" ? "Register Account" : "অ্যাকাউন্ট নিবন্ধন করুন"
        nameLabel.text = UserDefaults.standard.value(forKey: "lang") as! String == "1" ? "Your Name" : "আপনার নাম"
        nidLabel.text = UserDefaults.standard.value(forKey: "lang") as! String == "1" ? "National Id Number" : "জাতীয় পরিচয় পত্র নাম্বার"
        passportLabel.text = UserDefaults.standard.value(forKey: "lang") as! String == "1" ? "Passport Number" : "পাসপোর্টের নাম্বার"
        passportExpDateTxtLbl.text = UserDefaults.standard.value(forKey: "lang") as! String == "1" ? "Passport Expiration date" : "পাসপোর্টের মেয়াদোত্তীর্ণের তারিখ"
        emailAddressLabel.text = UserDefaults.standard.value(forKey: "lang") as! String == "1" ? "Your E-mail Address" : "আপনার ইমেইল এড্রেস"
        selectPasswordLabel.text = UserDefaults.standard.value(forKey: "lang") as! String == "1" ? "Select Password" : "পাসওয়ার্ড নির্বাচন করুন"
        mobileNumberLabel.text = UserDefaults.standard.value(forKey: "lang") as! String == "1" ? "Your mobile number" : "আপনার মোবাইল নাম্বার"
        serviceDeliveryStation.text = UserDefaults.standard.value(forKey: "lang") as! String == "1" ? "Service Delivery Station" : "সেবা প্রদানকারী মিশন"
        currentAddress.text = UserDefaults.standard.value(forKey: "lang") as! String == "1" ? "Current Address" : "বর্তমান ঠিকানা"
        addressLineLabel.text = UserDefaults.standard.value(forKey: "lang") as! String == "1" ? "Address(house/road/block)" : "ঠিকানা(বাসা/রাস্তা/ব্লক)"
        passposrtNidTitleLabel.text = UserDefaults.standard.value(forKey: "lang") as! String == "1" ? "Attach Scanned copy of Passport and NID" : "পাসপোর্ট এবং এনআইডির স্ক্যান কপি সংযুক্ত করুন"
        passportScanLable.text = UserDefaults.standard.value(forKey: "lang") as! String == "1" ? "Passport Scan copy" : "পাসপোর্ট স্ক্যান কপি"
        nidScanLable.text = UserDefaults.standard.value(forKey: "lang") as! String == "1" ? "NID Scan Copy" : "এনআইডি স্ক্যান কপি"
        let CompleteRegistrationTxt = UserDefaults.standard.value(forKey: "lang") as! String == "1" ? "Complete Registration" : "নিবন্ধন সম্পন্ন করুন"
        completeRegistrationBtn.setTitle(CompleteRegistrationTxt, for: .normal)
        
        let cancelDateBtnLableTxt = UserDefaults.standard.value(forKey: "lang") as! String == "1" ? "Cancel" : "বাতিল করুন"
        cancelDateBtnLable.setTitle(cancelDateBtnLableTxt, for: .normal)
        
        let selectDateBtnLableTxt = UserDefaults.standard.value(forKey: "lang") as! String == "1" ? "Select" : "নির্বাচন করুন"
        selectDateBtnLable.setTitle(selectDateBtnLableTxt, for: .normal)
        
        uploadProgressLable.text = UserDefaults.standard.value(forKey: "lang") as! String == "1" ? "We are creating your account, please wait. Depending on your internet connection it may take upto 2/3 minutes to complete the process." : "আপলোড সম্পন্ন হচ্ছে, দয়া করে অপেক্ষা করুন, আপনার ইন্টারনেট এর গতির উপর নির্ভর করে আপলোড সম্পন্ন হতে কয়েক মিনিট পর্যন্ত সময় প্রয়োজন হতে পারে"
        
        successMessageTitle.text = UserDefaults.standard.value(forKey: "lang") as! String == "1" ? "Thank You" : "ধন্যবাদ"
        
        successMessageBody.text = UserDefaults.standard.value(forKey: "lang") as! String == "1" ? "Your account has been created successfully" : "আপনার অ্যাকাউন্টটি সফলভাবে নিবন্ধিত হয়েছে"
        
        
        let startUsingAppBtnLableTxt = UserDefaults.standard.value(forKey: "lang") as! String == "1" ? "Start using Dutabash" : "ব্যবহার শুরু করুন"
        startUsingAppBtnLable.setTitle(startUsingAppBtnLableTxt, for: .normal)
        
        let cancelConsulateSelectionBtnLableTxt = UserDefaults.standard.value(forKey: "lang") as! String == "1" ? "Cacel" : "বাতিল করুন"
        
    cancelConsulateSelectionBtnLable.setTitle(cancelConsulateSelectionBtnLableTxt, for: .normal)
        
        let selectConsulateSelectionBtnLableTxt = UserDefaults.standard.value(forKey: "lang") as! String == "1" ? "Select" : "নির্বাচন করুন"
    selectConsulateSelectionBtnLable.setTitle(selectConsulateSelectionBtnLableTxt, for: .normal)
        
    }
    
    
    @IBAction func backBtnTapped(_ sender: Any) {
        
        
        
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func startUsingTapped(_ sender: Any) {

        let vc = UIStoryboard(name: "Home", bundle:nil).instantiateViewController(withIdentifier: "MaintabBarcontroller") as! MaintabBarcontroller;
        self.present(vc, animated: true, completion: nil)
        
        
    }

    @IBAction func datePiker(_ sender: Any) {
        
    }
    
    @IBAction func SelectConsulateTapped(_ sender: Any) {
        selectConsulateView.isHidden = false
    }
    
    
    @IBAction func passpostExpDateTapped(_ sender: Any) {
        datePickerView.isHidden = false
    }
    
    @IBOutlet weak var datePickerOutlet: UIDatePicker!

    
    @IBAction func selectDate(_ sender: Any) {
        datePickerView.isHidden = true
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        pickedDate = formatter.string(from: datePickerOutlet.date)
        passportExpDateTxtLbl.text = formatter.string(from: datePickerOutlet.date)
        print("pickedDate", pickedDate)
    }
    
    @IBAction func cancelDate(_ sender: Any) {
        datePickerView.isHidden = true
    }
    
    // picker view
    @IBAction func selectConsulateTapped(_ sender: Any) {
        
        let selectedPickerIndex = consularPicker.selectedRow(inComponent: 0)
        print(selectedPickerIndex)
        pickedConsulateID = userObject.missiontempArr[selectedPickerIndex].mission_id
        pickedConsulate = userObject.missiontempArr[selectedPickerIndex].mission_name + ", " + userObject.missiontempArr[selectedPickerIndex].city_name
        
        missionNameTxt.text = userObject.missiontempArr[selectedPickerIndex].mission_name + ", " + userObject.missiontempArr[selectedPickerIndex].city_name
        selectConsulateView.isHidden = true
        
    }
    
    
    @IBAction func cancelMissionSelectTapped(_ sender: Any) {
        
        selectConsulateView.isHidden = true
        
    }
    
    
    
    @objc func uploadProgressBar(notification: Notification){
        print("called_PG_view")
        progressView.setProgress(Float(uploadProgress), animated: true)
        
    }
    
    @IBAction func passportDocUpload(_ sender: Any) {
        
        tappFlag = 1
        
        let actionSheet = UIAlertController(title: UserDefaults.standard.value(forKey: "lang") as! String == "1" ? "Do you want to use your camera to scan your document or you want to upload previously scanned document from your phone" : "আপনি কি মোবাইল ক্যামেরার ব্যবহার করে স্ক্যান করতে চান? অথবা পূর্ববর্তীতে স্ক্যানকৃত পাসপোর্ট আপলোড করতে চান?", message: nil, preferredStyle: .actionSheet)
        
        let scanAction = UIAlertAction(title: UserDefaults.standard.value(forKey: "lang") as! String == "1" ? "Use Camera" : "ক্যামেরা ব্যবহার করুন", style: .default) { (_) in
            self.scanImage()
        }
        
        let selectAction = UIAlertAction(title: UserDefaults.standard.value(forKey: "lang") as! String == "1" ? "Upload File" : "ফাইল আপলোড করুন", style: .default) { (_) in
            self.selectImage()
        }
        
        let cancelAction = UIAlertAction(title: UserDefaults.standard.value(forKey: "lang") as! String == "1" ? "Cancel" : "বাতিল করুন", style: .cancel, handler: nil)
        
        actionSheet.addAction(scanAction)
        actionSheet.addAction(selectAction)
        actionSheet.addAction(cancelAction)
        
        present(actionSheet, animated: true)
    }
    
    @IBAction func nidDocUploaded(_ sender: Any) {
        
        tappFlag = 2
        
        let actionSheet = UIAlertController(title: UserDefaults.standard.value(forKey: "lang") as! String == "1" ? "Do you want to use your camera to scan your document or you want to upload previously scanned document from your phone" : "আপনি কি মোবাইল ক্যামেরার ব্যবহার করে স্ক্যান করতে চান? অথবা পূর্ববর্তীতে স্ক্যানকৃত জাতীয় পরিচয়পত্র আপলোড করতে চান?", message: nil, preferredStyle: .actionSheet)
        
        let scanAction = UIAlertAction(title: UserDefaults.standard.value(forKey: "lang") as! String == "1" ? "Use Camera" : "ক্যামেরা ব্যবহার করুন", style: .default) { (_) in
            self.scanImage()
        }
        
        let selectAction = UIAlertAction(title: UserDefaults.standard.value(forKey: "lang") as! String == "1" ? "Upload File" : "ফাইল আপলোড করুন", style: .default) { (_) in
            self.selectImage()
        }
        
        let cancelAction = UIAlertAction(title: UserDefaults.standard.value(forKey: "lang") as! String == "1" ? "Cancel" : "বাতিল করুন", style: .cancel, handler: nil)
        
        actionSheet.addAction(scanAction)
        actionSheet.addAction(selectAction)
        actionSheet.addAction(cancelAction)
        
        present(actionSheet, animated: true)
        
    }
    
    func scanImage() {
        let scannerViewController = ImageScannerController()
        scannerViewController.imageScannerDelegate = self
        present(scannerViewController, animated: true)
    }
    
    func selectImage() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        present(imagePicker, animated: true)
    }
    
    
    func imageScannerController(_ scanner: ImageScannerController, didFailWithError error: Error) {
        // You are responsible for carefully handling the error
        print(error)
    }
    
    func imageScannerController(_ scanner: ImageScannerController, didFinishScanningWithResults results: ImageScannerResults) {
        // The user successfully scanned an image, which is available in the ImageScannerResults
        // You are responsible for dismissing the ImageScannerController
        
        if tappFlag == 1{
            passportDocImage = results.scannedImage
            passportScannedImage.image = results.scannedImage
        }else{
            nidDocImage = results.scannedImage
            nidScannedImage.image = results.scannedImage
        }
        
        
        scanner.dismiss(animated: true)
    }
    
    func imageScannerControllerDidCancel(_ scanner: ImageScannerController) {
        // The user tapped 'Cancel' on the scanner
        // You are responsible for dismissing the ImageScannerController
        scanner.dismiss(animated: true)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            // imageViewPic.contentMode = .scaleToFill
            if tappFlag == 1{
                passportDocImage = pickedImage
                passportScannedImage.image = pickedImage
            }else{
                nidDocImage = pickedImage
                nidScannedImage.image = pickedImage
            }
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    
    func getServiceList(){
        
        let progressHUD = ProgressHUD(text: UserDefaults.standard.value(forKey: "lang") as! String == "1" ? "Loading..." : "লোড হচ্ছে…")
        self.view.addSubview(progressHUD)
        progressHUD.show()
        
        
        ServicesObject.getServices(category_id: "1", success: { (status, message) in
            
            print(status)
            print(message)
            
            if status == 200 {
                
                ServicesObject.getServicesHistoryByTypeStatus(status: 0, success: { (status, message) in
                    
                    print(status)
                    print(message)
                    
                    if status == 200 {
                        
                        MessageObject.getMessage(success: { (status, message) in
                            
                            print(status)
                            print(message)
                            
                            if status == 200 {
                                
                                SwiftEntryKit.dismiss()
                                progressHUD.hide()
                                self.successView.isHidden = false
                                
                            } else if status == 100{
                                
                                let title = UserDefaults.standard.value(forKey: "lang") as! String == "1" ? error_title_english : error_text_arabic
                                let desc = message
                                let image = "thoag_icon_white"
                                self.showNotificationMessage(attributes: self.dataSource[3, 1].attributes, title: title, desc: desc, textColor: .white, imageName: image)
                                
                            }
                            
                        }) { (message) in
                            
                            // Set note label content
                            let text = message
                            let style = EKProperty.LabelStyle(font: MainFont.light.with(size: 14), color: .white, alignment: .center)
                            let labelContent = EKProperty.LabelContent(text: text, style: style)
                            let imageContent = EKProperty.ImageContent(image: UIImage(named: "ic_wifi")!)
                            let contentView = EKImageNoteMessageView(with: labelContent, imageContent: imageContent)
                            SwiftEntryKit.display(entry: contentView, using: self.dataSource[1, 2].attributes)
                            
                        }
                        
                    } else if status == 100{
                        
                        let title = UserDefaults.standard.value(forKey: "lang") as! String == "1" ? error_title_english : error_text_arabic
                        let desc = message
                        let image = "thoag_icon_white"
                        self.showNotificationMessage(attributes: self.dataSource[3, 1].attributes, title: title, desc: desc, textColor: .white, imageName: image)
                        
                    }
                    
                }) { (message) in
                    
                    // Set note label content
                    let text = message
                    let style = EKProperty.LabelStyle(font: MainFont.light.with(size: 14), color: .white, alignment: .center)
                    let labelContent = EKProperty.LabelContent(text: text, style: style)
                    let imageContent = EKProperty.ImageContent(image: UIImage(named: "ic_wifi")!)
                    let contentView = EKImageNoteMessageView(with: labelContent, imageContent: imageContent)
                    SwiftEntryKit.display(entry: contentView, using: self.dataSource[1, 2].attributes)
                    
                }
                
                
                
            } else if status == 100{
                
                progressHUD.hide()
                let title = UserDefaults.standard.value(forKey: "lang") as! String == "1" ? error_title_english : error_text_arabic
                let desc = message
                let image = "thoag_icon_white"
                self.showNotificationMessage(attributes: self.dataSource[3, 1].attributes, title: title, desc: desc, textColor: .white, imageName: image)
                
            }
            
        }) { (message) in
            progressHUD.hide()
            
            // Set note label content
            let text = message
            let style = EKProperty.LabelStyle(font: MainFont.light.with(size: 14), color: .white, alignment: .center)
            let labelContent = EKProperty.LabelContent(text: text, style: style)
            let imageContent = EKProperty.ImageContent(image: UIImage(named: "ic_wifi")!)
            let contentView = EKImageNoteMessageView(with: labelContent, imageContent: imageContent)
            SwiftEntryKit.display(entry: contentView, using: self.dataSource[1, 2].attributes)
            
        }
        
    }
    
    @IBAction func createAccountTapped(_ sender: Any) {

        if nameTxtLbl.text!.isEmpty == true{
            showAlert(errorMessage: "Please type your name")
        }else if nidTxtLbl.text!.isEmpty == true{
            showAlert(errorMessage: "Please type your National Id Card Number")
        }else if passportTxtLbl.text!.isEmpty == true{
            showAlert(errorMessage: "Please type your passport number")
        }else if passportExpDateTxtLbl.text!.isEmpty == true{
            showAlert(errorMessage: "Please type your passport expiration date")
        }else if emailAddressTxtLbl.text!.isEmpty == true{
            showAlert(errorMessage: "Please type your E-Mail address")
        }else if passwordTxtLbl.text!.isEmpty == true{
            showAlert(errorMessage: "Please choose a password")
        }else if phoneNumber.text!.isEmpty == true{
            showAlert(errorMessage: "Please input your phone number")
        }else if nidDocImage == nil{
            showAlert(errorMessage: "Please attach scan copy of your National ID card")
        }else if passportDocImage == nil{
            showAlert(errorMessage: "Please attach scan copy of your Passport")
        }else{
            
            progressBlurView.isHidden = false
            let progressHUD = ProgressHUD(text: UserDefaults.standard.value(forKey: "lang") as! String == "1" ? "Loading..." : "লোড হচ্ছে…")
            self.view.addSubview(progressHUD)
            progressHUD.show()
            
            userObject.userSingup(present_address: paddressL1.text!, name: nameTxtLbl.text!, password: passwordTxtLbl.text!, email: emailAddressTxtLbl.text!, passport_number: passportTxtLbl.text!, passport_valid_date: passportExpDateTxtLbl.text!, nid_number: nidTxtLbl.text!, device_id: devicetokenUser, mission_id: userObject.missiontempArr[0].mission_id, nid_document: nidDocImage, passport_document: passportDocImage, success: { (status, message) in
                
                print(status)
                print(message)
                
                if status == 200 {
                    progressHUD.hide()
                    
                    self.getServiceList()
                    
                }else if status == 100{
                    progressHUD.hide()
                    
                    self.progressBlurView.isHidden = true
                    let title = UserDefaults.standard.value(forKey: "lang") as! String == "1" ? "Error" : "خطاء"
                    let desc = message
                    let image = "thoag_icon_white"
                    self.showNotificationMessage(attributes: self.dataSource[2, 0].attributes, title: title, desc: desc, textColor: .white, imageName: image)
                    
                }
                
            }) { (message) in
                progressHUD.hide()
                
                self.progressBlurView.isHidden = true
                // Set note label content
                let text = message
                let style = EKProperty.LabelStyle(font: MainFont.light.with(size: 14), color: .white, alignment: .center)
                let labelContent = EKProperty.LabelContent(text: text, style: style)
                let imageContent = EKProperty.ImageContent(image: UIImage(named: "ic_wifi")!)
                let contentView = EKImageNoteMessageView(with: labelContent, imageContent: imageContent)
                SwiftEntryKit.display(entry: contentView, using: self.dataSource[1, 2].attributes)
                
            }
            
        }
    }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return userObject.missiontempArr.count
    }

    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        // self.labelFruit.text = userObject.missiontempArr[row].mission_name
    }

    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let label: UILabel
        
        if let view = view {
            label = view as! UILabel
        }
        else {
            label = UILabel(frame: CGRect(x: 0, y: 0, width: ((pickerView.frame.width) - 20.0), height: 400))
        }
        
        label.text = userObject.missiontempArr[row].mission_name + ", " + userObject.missiontempArr[row].city_name
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 3
        label.sizeToFit()
        
        return label
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 80.0
    }

}

extension Double {
    func rounded(digits: Int) -> Double {
        let multiplier = pow(10.0, Double(digits))
        return (self * multiplier).rounded() / multiplier
    }
}
