//
//  Location.swift
//  Thoag
//
//  Created by Tusher on 2/26/19.
//  Copyright © 2019 Ashraf Ul Alam Tusher. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON


class LocationLocal{
    
    private var _location_id : Int!
    private var _address : String!
    private var _mission : String!
    private var _is_current : Int!
    private var _longitude : String!
    private var _latitude : String!
    
    init(){
        self._location_id = 0
        self._address = ""
        self._mission = ""
        self._is_current = 0
        self._longitude = ""
        self._latitude = ""
    }
    
    var location_id: Int{
        get{ return  _location_id }
        set(_location_id){
            self._location_id = _location_id
        }
    }
    
    var address: String{
        get{ return  _address }
        set(_address){
            self._address = _address
        }
    }
    
    var mission: String{
        get{ return  _mission }
        set(_mission){
            self._mission = _mission
        }
    }
    
    var is_current: Int{
        get{ return  _is_current }
        set(_is_current){
            self._is_current = _is_current
        }
    }
    
    var longitude: String{
        get{ return  _longitude }
        set(_longitude){
            self._longitude = _longitude
        }
    }
    
    var latitude: String{
        get{ return  _latitude }
        set(_latitude){
            self._latitude = _latitude
        }
    }
    
    var MissionArr = [Mission]()
    
}

