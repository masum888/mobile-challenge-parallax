import Foundation

/// This class is designed to check if the network is connected.
open class Reachability {
    
    /// This function will hit two urls that should never be totally down to see if the device is connected
    /// If either url connects, this returns true
    ///
    /// - Parameter resultHandler: returns the result of the connection existing as a Bool in a resultHandler
    class func isConnectedToNetwork(_ resultHandler: @escaping (_ isTrue: Bool) -> Void) {
        let urlA = URL(string: "https://google.com/")
        let urlB = URL(string: "https://baidu.com/")
        
        Reachability.fetchURL(urlA!) { isTrue in
            if isTrue {
                print("NETWORK STATUS: \(isTrue)")
                resultHandler(isTrue)
            } else {
                Reachability.fetchURL(urlB!, resultHandler: resultHandler)
            }
        }
    }
    
    /// Hits a URL in order to see if the device can connect to it
    ///
    /// - Parameters:
    ///   - url: the url to request
    ///   - resultHandler: returns whether the url was successfully retrieved or not
    class func fetchURL(_ url:URL, resultHandler: @escaping (_ isTrue: Bool) -> Void) {
        var request = URLRequest(url: url)
        request.httpMethod = "HEAD"
        request.cachePolicy = URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData
        request.timeoutInterval = 10.0
        
        let session = URLSession(configuration: .default)
        let dataTask = session.dataTask(with: request) { data, response, error in
            if let httpResponse = response as? HTTPURLResponse {
                if httpResponse.statusCode == 200 {
                    resultHandler(true)
                } else {
                    resultHandler(false)
                }
            } else {
                resultHandler(false)
            }
        }
        dataTask.resume()
    }
}
