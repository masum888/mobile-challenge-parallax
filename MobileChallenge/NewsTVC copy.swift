//
//  NewsTVC.swift
//  Dutabash
//
//  Created by Tusher on 4/7/19.
//  Copyright © 2019 Ashraf Ul Alam Tusher. All rights reserved.
//

import UIKit
import Kingfisher
import SwiftEntryKit

class PressReleaseTVC: MasterTableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return HomeObject.newsArr.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewsCell", for: indexPath) as! NewsCell
        cell.newsTitle.text = HomeObject.newsArr[indexPath.row].news_title
        cell.newsDate.text = HomeObject.newsArr[indexPath.row].news_date

        let urlstring = image_base_url + HomeObject.newsArr[indexPath.row].news_media_image
        print("imageurl", urlstring)
        let url = URL(string: urlstring)
        let image = UIImage(named: "placeholder")
        cell.newsThumb.kf.indicatorType = .activity
        cell.newsThumb.kf.setImage(with: url, placeholder: image)
        
        
        return cell
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        tabBarController?.tabBar.isHidden = false
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        let progressHUD = ProgressHUD(text: UserDefaults.standard.value(forKey: "lang") as! String == "1" ? "Loading..." : "লোড হচ্ছে…")
        self.view.addSubview(progressHUD)
        progressHUD.show()
        
        HomeObject.getNewsDetails(newsId: HomeObject.newsArr[indexPath.row].news_id, success: { (status, message) in
            
            print(status)
            print(message)
            
            if status == 200 {
                progressHUD.hide()
                selectedNewsItem = indexPath.row
                let vc = UIStoryboard(name: "Home", bundle:nil).instantiateViewController(withIdentifier: "NewsDetailsTVC") as! NewsDetailsTVC
                self.navigationController?.pushViewController(vc, animated: true)
                
            }else if status == 100{
                progressHUD.hide()
                let title = UserDefaults.standard.value(forKey: "lang") as! String == "1" ? "Error" : "خطاء"
                let desc = message
                let image = "thoag_icon_white"
                self.showNotificationMessage(attributes: self.dataSource[2, 0].attributes, title: title, desc: desc, textColor: .white, imageName: image)
                
            }
            
        }) { (message) in
            // Set note label content
            let text = message
            let style = EKProperty.LabelStyle(font: MainFont.light.with(size: 14), color: .white, alignment: .center)
            let labelContent = EKProperty.LabelContent(text: text, style: style)
            let imageContent = EKProperty.ImageContent(image: UIImage(named: "ic_wifi")!)
            let contentView = EKImageNoteMessageView(with: labelContent, imageContent: imageContent)
            SwiftEntryKit.display(entry: contentView, using: self.dataSource[1, 2].attributes)
            
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }

}
