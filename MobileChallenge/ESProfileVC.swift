//
//  ESProfileVC.swift
//  Ekshop
//
//  Created by Masum Ahmed on 26/1/21.
//  Copyright © 2021 Ashraf Ul Alam Tusher. All rights reserved.
//

import UIKit

class ESAccountVC: UIViewController {
    
    @IBOutlet weak var oiv: UIView!
    @IBOutlet weak var piv: UIView!
    @IBOutlet weak var aiv: UIView!
    @IBOutlet weak var miv: UIView!
    
    @IBOutlet weak var liv1: UIView!
    @IBOutlet weak var liv2: UIView!
    @IBOutlet weak var liv3: UIView!
    @IBOutlet weak var liv4: UIView!
    @IBOutlet weak var liv5: UIView!
    @IBOutlet weak var liv6: UIView!
    @IBOutlet weak var liv7: UIView!
    @IBOutlet weak var liv8: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        oiv.layer.cornerRadius = oiv.frame.height / 2
        piv.layer.cornerRadius = piv.frame.width / 2
        aiv.layer.cornerRadius = aiv.frame.width / 2
        miv.layer.cornerRadius = miv.frame.width / 2
        liv1.layer.cornerRadius = liv1.frame.width / 2
        liv2.layer.cornerRadius = liv2.frame.width / 2
        liv3.layer.cornerRadius = liv3.frame.width / 2
        liv4.layer.cornerRadius = liv4.frame.width / 2
        liv5.layer.cornerRadius = liv5.frame.width / 2
        liv6.layer.cornerRadius = liv6.frame.width / 2
        liv7.layer.cornerRadius = liv7.frame.width / 2
        liv8.layer.cornerRadius = liv8.frame.width / 2

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
