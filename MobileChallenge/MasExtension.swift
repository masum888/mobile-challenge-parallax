//
//  MasExtension.swift
//  Thoag
//
//  Created by Masum Ahmed on 27/11/19.
//  Copyright © 2019 Ashraf Ul Alam Tusher. All rights reserved.
//

import UIKit
import Alamofire
import Kingfisher
import NVActivityIndicatorView
import ImageIO


extension UIFont {
    class var chatFont: UIFont {
        return UIFont(name: "Helvetica", size: 12.0) ?? UIFont()
    }
    
    class var normalFont: UIFont {
        return UIFont (name: "HelveticaNeue", size: 15)!
    }
    
    class var mediumFont: UIFont {
        return UIFont (name: "HelveticaNeue-Medium", size: 15)!
    }
    
    class var lightFont: UIFont {
        return UIFont (name: "HelveticaNeue-Light", size: 14)!
    }
}


/// Delete All UserDefaults Data
func deleteUserDefaultsData() {
    UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
    UserDefaults.standard.synchronize()
}

//Retrieve Image From Given URL
func retrieveImage(imageURL: String, placeHolderImage: UIImage = #imageLiteral(resourceName: "recaipientName"), completion: @escaping (_ image: UIImage) -> Void) {
    
    if imageURL != "" {
        
        guard let url = URL(string: imageURL) else {
            return
        }
        //KingfisherManager.shared.cache.clearDiskCache()
        KingfisherManager.shared.retrieveImage(with: url, options: [.transition(.fade(1))], progressBlock: nil) { (image, error, cacheType, url) in
            
            if image != nil {
                completion(image!)
            }else{
                completion(placeHolderImage)
            }
        }
        
    }else{
        completion(placeHolderImage)
    }
}

extension String {
    
    /* usages of getAttributedString
     // Background Color
     let attributedString = "YOUR_STRING".getAttributedString(.backgroundColor, value: UIColor.blue)

     // Forground Color
     let attributedString = "YOUR_STRING".getAttributedString(.foregroundColor, value: UIColor.red)

     // Font
     let attributedString = "YOUR_STRING".getAttributedString(.font, value: UIFont.boldSystemFont(ofSize: 15))

     // Underline
     let attributedString = "YOUR_STRING".getAttributedString(.underlineStyle, value: NSUnderlineStyle.single)

     // Underline Color
     let attributedString = "YOUR_STRING".getAttributedString(.underlineColor, value: UIColor.black)

         
     // set attributed text on a UILabel
     yourLabel.attributedText = attributedString
    */
    func getAttributedString<T>(_ key: NSAttributedString.Key, value: T) -> NSAttributedString {
       let applyAttribute = [ key: T.self ]
       let attrString = NSAttributedString(string: self, attributes: applyAttribute)
       return attrString
    }
    
    
    // myLabel.attributedText = "my string".strikeThrough()
    func strikeThrough() -> NSAttributedString {
        let attributeString =  NSMutableAttributedString(string: self)
        attributeString.addAttribute(
            NSAttributedString.Key.strikethroughStyle,
            value: NSUnderlineStyle.styleSingle.rawValue,
                   range:NSMakeRange(0,attributeString.length))
        return attributeString
    }
    
    
    // Check for Password Validation
    func isValidPassword() -> Bool {
        
        //With atleast one special char
        //let passwordRegEx = "(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9])(?=.*[a-z]).{8,}"
        
        let passwordRegEx = "(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z]).{8,}"
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", passwordRegEx)
        return passwordTest.evaluate(with: self)
    }
    
    // Check for Valid Email Address
    func isValidEmail2() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    // Check for Valid Contact Number
    func isValidContactNumber() -> Bool {
        return self.trimming().count > 7 && self.trimming().count <= 12
    }
    
    // Check for String is Empty
    func isEmpty() -> Bool {
        return self.trimming().isEmpty
    }
    
    // Return the string after trimming
    func trimming() -> String {
        let strText = self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        return strText
    }
    
    func condenseWhitespace() -> String {
        let components = self.components(separatedBy: NSCharacterSet.whitespacesAndNewlines)
        return components.filter { !$0.isEmpty }.joined(separator: " ")
    }
    
    var encodeEmoji: String? {
        let encodedStr = NSString(cString: self.cString(using: String.Encoding.nonLossyASCII)!, encoding: String.Encoding.utf8.rawValue)
        return encodedStr as String?
    }
    
    var decodeEmoji: String {
        let data = self.data(using: String.Encoding.utf8, allowLossyConversion: false)
        if data != nil {
            let valueUniCode = NSString(data: data!, encoding: String.Encoding.nonLossyASCII.rawValue) as String?
            if valueUniCode != nil {
                return valueUniCode!
            } else {
                return self
            }
        } else {
            return self
        }
    }
    
    var html2AttributedString: NSAttributedString? {
        return Data(utf8).html2AttributedString
    }
    
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
    
    func convertISODateIntoString(formatString: String) -> String {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        let dt = formatter.date(from: self) ?? nil
        if dt != nil {
            let stdt = dt!.convertDateToString(formatString: formatString)
            return stdt
        }
        return ""
    }
    
    func convertStringDateIntoFormattedStringDate(givenFormat: String, formatString: String) -> String {
        
        let formatter = DateFormatter()
        formatter.dateFormat = givenFormat
        let dt = formatter.date(from: self) ?? nil
        if dt != nil {
            let stdt = dt!.convertDateToString(formatString: formatString)
            return stdt
        }
        return ""
    }
    
    //Convert String to Date with Given Format
    func convertStringToDate(formatString: String) -> Date? {
        let formatter = DateFormatter()
        formatter.dateFormat = formatString
        
        return formatter.date(from: self) ?? nil
    }
    
    func getDate(formate: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formate
        
        return dateFormatter.date(from: self)
    }
    
    //Convert String Timestamp to Date with Given Format
    func getDate(timeFormat: String = "hh:mm a", dateTimeFormat: String = "dd-MM-yyyy hh:mm a") -> String {
        if self.isEmpty {
            return ""
        }
        
        let date = Date(timeIntervalSince1970: TimeInterval(UInt64(self)! / 1000))
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current
        
        if date.isSameDate() {
            dateFormatter.dateFormat = timeFormat
        }else{
            dateFormatter.dateFormat = dateTimeFormat
        }
        
        let localDate = dateFormatter.string(from: date)
        
        return localDate
    }
    
    //Get Only day of the Given
    func getOnlyDay(dateTimeFormat: String = "EEE", isMinusOneDay: Bool = false) -> String {
        if self.isEmpty {
            return ""
        }
        
        var date = Date(timeIntervalSince1970: TimeInterval(UInt64(self)! / 1000))
        
        if isMinusOneDay {
            date = date.dateBySubstractingDays(days: 1) ?? date
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = dateTimeFormat
        
        let localDate = dateFormatter.string(from: date)
        
        return localDate
    }
    
    func getDateFromTimeStamp() -> Date? {
        if self.isEmpty {
            return nil
        }
        
        return Date(timeIntervalSince1970: TimeInterval(UInt64(self)! / 1000))
    }
    
    
    //Check entered value is valid double number
    func isValidDouble(maxDecimalPlaces: Int) -> Bool {
        // Use NumberFormatter to check if we can turn the string into a number
        // and to get the locale specific decimal separator.
        let formatter = NumberFormatter()
        formatter.allowsFloats = true // Default is true, be explicit anyways
        let decimalSeparator = formatter.decimalSeparator ?? "."  // Gets the locale specific decimal separator. If for some reason there is none we assume "." is used as separator.
        
        // Check if we can create a valid number. (The formatter creates a NSNumber, but
        // every NSNumber is a valid double, so we're good!)
        if formatter.number(from: self) != nil {
            // Split our string at the decimal separator
            let split = self.components(separatedBy: decimalSeparator)
            
            // Depending on whether there was a decimalSeparator we may have one
            // or two parts now. If it is two then the second part is the one after
            // the separator, aka the digits we care about.
            // If there was no separator then the user hasn't entered a decimal
            // number yet and we treat the string as empty, succeeding the check
            let digits = split.count == 2 ? split.last ?? "" : ""
            
            // Finally check if we're <= the allowed digits
            return digits.count <= maxDecimalPlaces
        }
        
        return false // couldn't turn string into a valid number
    }
}

extension Data {
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print("error:", error)
            return  nil
        }
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}

extension NSAttributedString {
    var trimmed: NSAttributedString {
        let modifiedString = NSMutableAttributedString(attributedString: self)
        modifiedString.trimCharactersInSet(charSet: .whitespacesAndNewlines)
        return NSAttributedString(attributedString: modifiedString)
    }
}

extension NSMutableAttributedString {
    public func trimCharactersInSet(charSet: CharacterSet) {
        var range = (string as NSString).rangeOfCharacter(from: charSet)
        
        // Trim leading characters from character set.
        while range.length != 0 && range.location == 0 {
            replaceCharacters(in: range, with: "")
            range = (string as NSString).rangeOfCharacter(from: charSet)
        }
        
        // Trim trailing characters from character set.
        range = (string as NSString).rangeOfCharacter(from: charSet, options: .backwards)
        while range.length != 0 && NSMaxRange(range) == length {
            replaceCharacters(in: range, with: "")
            range = (string as NSString).rangeOfCharacter(from: charSet, options: .backwards)
        }
    }
}


extension Date {
    
    //return Time from the Date
    var time: Time {
        return Time(self)
    }
    
    // Convert local time to UTC (or GMT)
    func toGlobalTime() -> Date {
        let timezone = TimeZone.current
        let seconds = -TimeInterval(timezone.secondsFromGMT(for: self))
        return Date(timeInterval: seconds, since: self)
    }
    
    // Convert UTC (or GMT) to local time
    func toLocalTime() -> Date {
        let timezone = TimeZone.current
        let seconds = TimeInterval(timezone.secondsFromGMT(for: self))
        return Date(timeInterval: seconds, since: self)
    }
    
    //Convert Date to String with Given Format
    func convertDateToString(formatString: String, timeZone: String? = nil) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = formatString
        if let timeZone = timeZone {
            formatter.timeZone = TimeZone(identifier: timeZone)
        }
        return formatter.string(from: self)
    }
    
    var day: Int {
        return Calendar.current.component(.day, from: self)
    }
    
    var month: Int {
        return Calendar.current.component(.month, from: self)
    }
    
    var year: Int {
        return Calendar.current.component(.year, from: self)
    }
    
    var daysInMonth: Int {
        let range = Calendar.current.range(of: .day, in: .month, for: self)!
        return range.count
    }
    
    func getWeekDayNumber() -> Int {
        return Calendar.current.component(.weekday, from: self)
    }
    
    func getDayName(formate: String = "EEEE") -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = formate
        
        return dateFormatter.string(from: self)
    }
    
    func dateByAddingDays(days: Int) -> Date? {
        return self.days(days: days) ?? nil
    }
    
    func dateBySubstractingDays(days: Int) -> Date? {
        return self.days(days: -days) ?? nil
    }
    
    func days(days:Int) -> Date? {
        return Calendar.current.date(byAdding: .day, value: days, to: self) ?? nil
    }
    
    func dateByAddingMonths(months: Int) -> Date? {
        return self.months(months: months) ?? nil
    }
    
    func dateBySubstractinMonths(months: Int) -> Date? {
        return self.months(months: -months) ?? nil
    }
    
    func months(months:Int) -> Date? {
        return Calendar.current.date(byAdding: .month, value: months, to: self) ?? nil
    }
    
    func dateByAddingYear(years: Int) -> Date? {
        return self.years(years: years) ?? nil
    }
    
    func dateBySubstractinYear(years: Int) -> Date? {
        return self.years(years: -years) ?? nil
    }
    
    func years(years:Int) -> Date? {
        return Calendar.current.date(byAdding: .year, value: years, to: self) ?? nil
    }
    
    func combineDateWithTime(time: Date) -> Date? {
        let calendar = NSCalendar.current
        
        let dateComponents = calendar.dateComponents([.year, .month, .day], from: self)
        let timeComponents = calendar.dateComponents([.hour, .minute, .second], from: time)
        
        var mergedComponments = DateComponents()
        mergedComponments.year = dateComponents.year!
        mergedComponments.month = dateComponents.month!
        mergedComponments.day = dateComponents.day!
        mergedComponments.hour = timeComponents.hour!
        mergedComponments.minute = timeComponents.minute!
        mergedComponments.second = timeComponents.second!
        
        return calendar.date(from: mergedComponments)
    }
    
    func combineDateWithDay(day: Date) -> Date? {
        let calendar = NSCalendar.current
        
        let dateComponents = calendar.dateComponents([.year, .month, .day], from: self)
        let timeComponents = calendar.dateComponents([.hour, .minute, .second], from: day)
        
        var mergedComponments = DateComponents()
        mergedComponments.year = timeComponents.year!
        mergedComponments.month = timeComponents.month!
        mergedComponments.day = timeComponents.day!
        mergedComponments.hour = dateComponents.hour!
        mergedComponments.minute = dateComponents.minute!
        mergedComponments.second = dateComponents.second!
        
        return calendar.date(from: mergedComponments)
    }
    
    func combineDateWithOnlyDay(day: Int) -> Date? {
        let calendar = NSCalendar.current
        
        let dateComponents = calendar.dateComponents([.year, .month, .day, .hour, .minute, .second], from: self)
        
        var mergedComponments = DateComponents()
        mergedComponments.year = dateComponents.year!
        mergedComponments.month = dateComponents.month!
        mergedComponments.day = day
        mergedComponments.hour = dateComponents.hour!
        mergedComponments.minute = dateComponents.minute!
        mergedComponments.second = dateComponents.second!
        
        return calendar.date(from: mergedComponments)
    }
    
    func combineDateWithMonthDay(month: Int, day: Int) -> Date? {
        let calendar = NSCalendar.current
        
        let dateComponents = calendar.dateComponents([.year, .month, .day, .hour, .minute, .second], from: self)
        
        var mergedComponments = DateComponents()
        mergedComponments.year = dateComponents.year!
        mergedComponments.month = month
        mergedComponments.day = day
        mergedComponments.hour = dateComponents.hour!
        mergedComponments.minute = dateComponents.minute!
        mergedComponments.second = dateComponents.second!
        
        return calendar.date(from: mergedComponments)
    }
    
    /// Returns the amount of years from another date
    func years(from date: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }
    /// Returns the amount of months from another date
    func months(from date: Date) -> Int {
        return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
    }
    /// Returns the amount of weeks from another date
    func weeks(from date: Date) -> Int {
        return Calendar.current.dateComponents([.weekOfMonth], from: date, to: self).weekOfMonth ?? 0
    }
    /// Returns the amount of days from another date
    func days(from date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
    }
    /// Returns the amount of hours from another date
    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    /// Returns the amount of minutes from another date
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    /// Returns the amount of seconds from another date
    func seconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
    }
    /// Returns the amount of nanoseconds from another date
    func nanoseconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.nanosecond], from: date, to: self).nanosecond ?? 0
    }
    /// Returns the a custom time interval description from another date
    func offset(from date: Date) -> String {
        if years(from: date)   > 0 { return "\(years(from: date))y"   }
        if months(from: date)  > 0 { return "\(months(from: date))M"  }
        if weeks(from: date)   > 0 { return "\(weeks(from: date))w"   }
        if days(from: date)    > 0 { return "\(days(from: date))d"    }
        if hours(from: date)   > 0 { return "\(hours(from: date))h"   }
        if minutes(from: date) > 0 { return "\(minutes(from: date))m" }
        if seconds(from: date) > 0 { return "\(seconds(from: date))s" }
        if nanoseconds(from: date) > 0 { return "\(nanoseconds(from: date))ns" }
        return ""
    }
    
    //TimeStamps
    var ticks: UInt64 {
        return UInt64(self.timeIntervalSince1970 * 1000)
    }
    
    func isSameDate(date: Date = Date()) -> Bool {
        let order = Calendar.current.compare(date, to: self, toGranularity: .day)
        return order == .orderedSame
    }
    
    func isBeforeDate() -> Bool {
        let order = Calendar.current.compare(Date(), to: self, toGranularity: .day)
        return order == .orderedAscending
    }
    
    func isAfterDate() -> Bool {
        let order = Calendar.current.compare(Date(), to: self, toGranularity: .day)
        return order == .orderedDescending
    }
    
    func timeAgoSinceDate() -> String {

        // From Time
        let fromDate = self

        // To Time
        let toDate = Date()

        // Estimation
        // Year
        if let interval = Calendar.current.dateComponents([.year], from: fromDate, to: toDate).year, interval > 0  {

            return interval == 1 ? "\(interval)" + " " + "year ago" : "\(interval)" + " " + "years ago"
        }

        // Month
        if let interval = Calendar.current.dateComponents([.month], from: fromDate, to: toDate).month, interval > 0  {

            return interval == 1 ? "\(interval)" + " " + "month ago" : "\(interval)" + " " + "months ago"
        }

        // Day
        if let interval = Calendar.current.dateComponents([.day], from: fromDate, to: toDate).day, interval > 0  {

            return interval == 1 ? "\(interval)" + " " + "day ago" : "\(interval)" + " " + "days ago"
        }

        // Hours
        if let interval = Calendar.current.dateComponents([.hour], from: fromDate, to: toDate).hour, interval > 0 {

            return interval == 1 ? "\(interval)" + " " + "hour ago" : "\(interval)" + " " + "hours ago"
        }

        // Minute
        if let interval = Calendar.current.dateComponents([.minute], from: fromDate, to: toDate).minute, interval > 0 {

            return interval == 1 ? "\(interval)" + " " + "minute ago" : "\(interval)" + " " + "minutes ago"
        }

        return "a moment ago"
    }
    
    func timeAgoSinceDate2() -> String {

        // From Time
        let fromDate = self

        // To Time
        let toDate = Date()

        // Estimation
        // Year
        if let interval = Calendar.current.dateComponents([.year], from: fromDate, to: toDate).year, interval > 0  {

            return interval == 1 ? "\(interval)" + " " + "year" : "\(interval)" + " " + "years"
        }

        // Month
        if let interval = Calendar.current.dateComponents([.month], from: fromDate, to: toDate).month, interval > 0  {

            return interval == 1 ? "\(interval)" + " " + "month" : "\(interval)" + " " + "months"
        }

        // Day
        if let interval = Calendar.current.dateComponents([.day], from: fromDate, to: toDate).day, interval > 0  {

            return interval == 1 ? "\(interval)" + " " + "day" : "\(interval)" + " " + "days"
        }

        // Hours
        if let interval = Calendar.current.dateComponents([.hour], from: fromDate, to: toDate).hour, interval > 0 {

            return interval == 1 ? "\(interval)" + " " + "hr" : "\(interval)" + " " + "hrs"
        }

        // Minute
        if let interval = Calendar.current.dateComponents([.minute], from: fromDate, to: toDate).minute, interval > 0 {

            return interval == 1 ? "\(interval)" + " " + "min" : "\(interval)" + " " + "mins"
        }

        return "just now"
    }
    func timeAgoSinceDate3() -> String {

        // From Time
        let fromDate = self

        // To Time
        let toDate = Date()

        // Estimation
        // Year
        if let interval = Calendar.current.dateComponents([.year], from: fromDate, to: toDate).year, interval > 0  {

            return interval == 1 ? "\(interval)" + " " + "year ago" : "\(interval)" + " " + "years ago"
        }

        // Month
        if let interval = Calendar.current.dateComponents([.month], from: fromDate, to: toDate).month, interval > 0  {

            return interval == 1 ? "\(interval)" + " " + "month ago" : "\(interval)" + " " + "months ago"
        }

        // Day
        if let interval = Calendar.current.dateComponents([.day], from: fromDate, to: toDate).day, interval > 0  {

            return interval == 1 ? "\(interval)" + " " + "day ago" : "\(interval)" + " " + "days ago"
        }

        // Hours
        if let interval = Calendar.current.dateComponents([.hour], from: fromDate, to: toDate).hour, interval > 0 {

            let df = DateFormatter()
            df.dateFormat = "h:mm a"
            let date = df.string(from: fromDate)
            return date
        }

        let df = DateFormatter()
        df.dateFormat = "h:mm a"
        let date = df.string(from: fromDate)
        return date
    }
}

extension UIView {
    
    @IBInspectable
    var cornerRadius2: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    @IBInspectable
    var borderWidth2: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable
    var borderColor2: UIColor? {
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
    
    @IBInspectable
    var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowColor = #colorLiteral(red: 0.4739874601, green: 0.5425413847, blue: 0.6254249811, alpha: 1).cgColor
            layer.shadowOffset = CGSize(width: 0, height: 2)
            layer.shadowOpacity = 0.2
            layer.shadowRadius = shadowRadius
            layer.masksToBounds = false
        }
    }
    
    
    //Give Corner Radious To Any Of the side
    //theView.roundCorners(corners: [.topLeft, .topRight], radius: 30)
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    
    //Add Gradient Layer
    func addThemeGradientToBackground(color1: UIColor = #colorLiteral(red: 0.2823529412, green: 0.8431372549, blue: 0.768627451, alpha: 1), color2: UIColor = #colorLiteral(red: 0.3254901961, green: 0.7725490196, blue: 0.9607843137, alpha: 1)) {
        let layer = CAGradientLayer()
        layer.frame = self.bounds
        layer.colors = [color1.cgColor, color2.cgColor]
        layer.startPoint = CGPoint(x: 0, y: 0)
        layer.endPoint = CGPoint(x: 1, y: 1)
        self.layer.insertSublayer(layer, at: 0)
    }
    
    //Add Gradient Layer
    func addTheme2GradientToBackground(color1: UIColor = #colorLiteral(red: 0.07843137255, green: 0.1568627451, blue: 0.2, alpha: 1), color2: UIColor = #colorLiteral(red: 0.01960784314, green: 0.2901960784, blue: 0.3764705882, alpha: 1)) {
        let layer = CAGradientLayer()
        layer.frame = self.bounds
        layer.colors = [color1.cgColor, color2.cgColor]
        layer.startPoint = CGPoint(x: 0, y: 0)
        layer.endPoint = CGPoint(x: 1, y: 1)
        self.layer.insertSublayer(layer, at: 0)
    }
    
    //Add Gradient Layer
    func addTheme3GradientToBackground(color1: UIColor = #colorLiteral(red: 0.7921568627, green: 0.9607843137, blue: 0.9411764706, alpha: 1), color2: UIColor = #colorLiteral(red: 0.9254901961, green: 0.9882352941, blue: 0.9803921569, alpha: 1)) {
        let layer = CAGradientLayer()
        layer.frame = self.bounds
        layer.colors = [color1.cgColor, color2.cgColor]
        layer.startPoint = CGPoint(x: 0, y: 0)
        layer.endPoint = CGPoint(x: 1, y: 1)
        self.layer.insertSublayer(layer, at: 0)
    }
    
    func addTheme4GradientToBackground(color1: UIColor = #colorLiteral(red: 0.7921568627, green: 0.9607843137, blue: 0.9411764706, alpha: 1), color2: UIColor = #colorLiteral(red: 0.9254901961, green: 0.9882352941, blue: 0.9803921569, alpha: 1)) {
        let layer = CAGradientLayer()
        layer.frame = self.bounds
        layer.colors = [color1.cgColor, color2.cgColor]
        layer.startPoint = CGPoint(x: 0, y: 1)
        layer.endPoint = CGPoint(x: 1, y: 1)
        self.layer.insertSublayer(layer, at: 0)
    }
}

extension UIImageView {
    
    // Download image and set into given imageview
    func setImage(imageURL: String, placeholderImage: UIImage = #imageLiteral(resourceName: "camera")) {
        if imageURL != "" {
            
            let url = URL(string: imageURL    )
            self.image = nil
            
            self.kf.indicatorType = .activity
            
            self.kf.setImage(with: url, placeholder: placeholderImage,
                             options: [.transition(.fade(1))],
                             progressBlock: nil,
                             completionHandler: nil)
        } else {
            self.image = placeholderImage
        }
    }
    
    // Download image and set into given imageview
    func setImageCache(imageURL: String, placeholderImage: UIImage = #imageLiteral(resourceName: "iconSmall")) {
        if imageURL != "" {
            
            let url = URL(string: imageURL)
            self.image = nil
            
            self.kf.indicatorType = .activity
            
            self.kf.setImage(with: url, placeholder: placeholderImage,
                             options: [.transition(.fade(1)),.cacheOriginalImage],
                             progressBlock: nil,
                             completionHandler: nil)
        } else {
            self.image = placeholderImage
        }
    }
    
    func setImageCache2(imageURL: String, placeholderImage: UIImage = #imageLiteral(resourceName: "iconSmall"), completion: @escaping (UIImage) -> ()) {
        if imageURL != "" {
            
            let url = URL(string: imageURL)
            self.image = nil
            
            self.kf.indicatorType = .activity
            
            self.kf.setImage(with: url, placeholder: placeholderImage,
                             options: [.transition(.fade(1)),.cacheOriginalImage],
                             progressBlock: nil,
                             completionHandler: { (image,error,cache,url) -> Void in
                                if image != nil {
                                    completion(image!)
                                }
                             })
            
        } else {
            self.image = placeholderImage
        }
    }
    
    func setProfileImageCache(imageURL: String, placeholderImage: UIImage = UIImage(named: "so-profile-edit-photo")! ) {
        if imageURL != "" {
            
            let url = URL(string: imageURL)
            self.image = nil
            
            self.kf.indicatorType = .activity
            
            self.kf.setImage(with: url, placeholder: placeholderImage,
                             options: [.transition(.fade(1)),.cacheOriginalImage],
                             progressBlock: nil,
                             completionHandler: nil)
        } else {
            self.image = placeholderImage
        }
    }
    

}

extension UILabel {
    func textWidth() -> CGFloat {
        return UILabel.textWidth(label: self)
    }
    
    class func textWidth(label: UILabel) -> CGFloat {
        return textWidth(label: label, text: label.text!)
    }
    
    class func textWidth(label: UILabel, text: String) -> CGFloat {
        return textWidth(font: label.font, text: text)
    }
    
    class func textWidth(font: UIFont, text: String) -> CGFloat {
        let myText = text as NSString
        
        let rect = CGSize(width: 50, height: CGFloat.greatestFiniteMagnitude)
        let labelSize = myText.boundingRect(with: rect, options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil)
        return ceil(labelSize.height)
    }
    
}

/* when tableview is inside stackview */
class IntrinsicTableView: UITableView {
    
    override var contentSize:CGSize {
        didSet {
            self.invalidateIntrinsicContentSize()
        }
    }
    
    
    override var intrinsicContentSize: CGSize {
        self.layoutIfNeeded()
        return CGSize(width: UIViewNoIntrinsicMetric, height: contentSize.height)
    }
    
}


extension UIColor {
    
    // Set RGB color for given HexaVaue
    convenience init(hex: Int) {
        let components = (
            R: CGFloat((hex >> 16) & 0xff) / 255,
            G: CGFloat((hex >> 08) & 0xff) / 255,
            B: CGFloat((hex >> 00) & 0xff) / 255
        )
        self.init(red: components.R, green: components.G, blue: components.B, alpha: 1)
    }
    
    class var loaderWhiteColor: UIColor {
        return #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    }
    
    class var loaderThemeColor: UIColor {
        return #colorLiteral(red: 0.2274509804, green: 0.7098039216, blue: 0.9137254902, alpha: 1)
    }
    
    class var collectionViewBlankLabelColor: UIColor {
        return #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }
    
    class var tableViewBlankLabelColor: UIColor {
        return #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }
    
    class var tableViewPagerLoaderColor: UIColor {
        return #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }
    
    class var tableViewPullToRefereshColor: UIColor {
        return #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }
    
    class var chartDataNotAvailabelColor: UIColor {
        return #colorLiteral(red: 0.9137254902, green: 0.7411764706, blue: 0.4666666667, alpha: 1)
    }
}

// Check Network Rechable
var isReachable: Bool {
    return NetworkReachabilityManager()!.isReachable
}



class Time: Comparable, Equatable {
    init(_ date: Date) {
        //get the current calender
        let calendar = Calendar.current
        
        //get just the minute and the hour of the day passed to it
        let dateComponents = calendar.dateComponents([.hour, .minute], from: date)
        
        //calculate the seconds since the beggining of the day for comparisions
        let dateSeconds = dateComponents.hour! * 3600 + dateComponents.minute! * 60
        
        //set the varibles
        secondsSinceBeginningOfDay = dateSeconds
        hour = dateComponents.hour!
        minute = dateComponents.minute!
    }
    
    init(_ hour: Int, _ minute: Int) {
        //calculate the seconds since the beggining of the day for comparisions
        let dateSeconds = hour * 3600 + minute * 60
        
        //set the varibles
        secondsSinceBeginningOfDay = dateSeconds
        self.hour = hour
        self.minute = minute
    }
    
    var hour : Int
    var minute: Int
    
    var date: Date {
        //get the current calender
        let calendar = Calendar.current
        
        //create a new date components.
        var dateComponents = DateComponents()
        
        dateComponents.hour = hour
        dateComponents.minute = minute
        
        return calendar.date(byAdding: dateComponents, to: Date())!
    }
    
    /// the number or seconds since the beggining of the day, this is used for comparisions
    private let secondsSinceBeginningOfDay: Int
    
    //comparisions so you can compare times
    static func == (lhs: Time, rhs: Time) -> Bool {
        return lhs.secondsSinceBeginningOfDay == rhs.secondsSinceBeginningOfDay
    }
    
    static func < (lhs: Time, rhs: Time) -> Bool {
        return lhs.secondsSinceBeginningOfDay < rhs.secondsSinceBeginningOfDay
    }
    
    static func <= (lhs: Time, rhs: Time) -> Bool {
        return lhs.secondsSinceBeginningOfDay <= rhs.secondsSinceBeginningOfDay
    }
    
    
    static func >= (lhs: Time, rhs: Time) -> Bool {
        return lhs.secondsSinceBeginningOfDay >= rhs.secondsSinceBeginningOfDay
    }
    
    
    static func > (lhs: Time, rhs: Time) -> Bool {
        return lhs.secondsSinceBeginningOfDay > rhs.secondsSinceBeginningOfDay
    }
}

extension UIViewController: NVActivityIndicatorViewable {
    // Show LoadingView When API is called
    func showLoading(_ color: UIColor) {
        let size = CGSize(width: 40, height:40)
        startAnimating(size, message: nil, type: .ballPulseSync, color: color, backgroundColor: .clear)
    }
    
    // Hide LoadingView
    func hideLoading() {
        stopAnimating()
    }
    
    // Hide Navigation Bar
    func hideNavigationBar() {
        navigationController!.setNavigationBarHidden(true, animated: false)
    }
    
    // Show Navigation Bar
    func showNavigationBar() {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    // Show Back Button when Slide to to Back
    func showBackButton() {
        navigationItem.hidesBackButton = false
    }
    
    // Hide Back Button when Slide to to Back
    func hideBackButton() {
        navigationItem.hidesBackButton = true
    }

    
    // Give Alpha Animation to the Selected View
    func setAlphaAnimation(selectedView: UIView, alpha: CGFloat) {
        if alpha == 1 {
            selectedView.isHidden = false
        }
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            selectedView.alpha = alpha
        }) {
            (complete) -> Void in
            if alpha == 0 {
                selectedView.isHidden = true
            }
        }
    }
    
    // Filter and return the Picker Data from the given array
    func getPickerData(arrayPickerData: [MasPickerData], value: String) -> MasPickerData? {
        let pickerData = arrayPickerData.filter({ $0.value.lowercased() == value.lowercased() })
        
        return pickerData.count > 0 ? pickerData[0] : nil
    }
}

extension UIImage {
    func rotate(radians: Float) -> UIImage? {
        var newSize = CGRect(origin: CGPoint.zero, size: self.size).applying(CGAffineTransform(rotationAngle: CGFloat(radians))).size
        // Trim off the extremely small float value to prevent core graphics from rounding it up
        newSize.width = floor(newSize.width)
        newSize.height = floor(newSize.height)
        
        UIGraphicsBeginImageContextWithOptions(newSize, false, self.scale)
        let context = UIGraphicsGetCurrentContext()!
        
        // Move origin to middle
        context.translateBy(x: newSize.width/2, y: newSize.height/2)
        // Rotate around middle
        context.rotate(by: CGFloat(radians))
        // Draw the image at its center
        self.draw(in: CGRect(x: -self.size.width/2, y: -self.size.height/2, width: self.size.width, height: self.size.height))
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    func upOrientationImageBeforeSaveToLocalDisk() -> UIImage? {
        switch imageOrientation {
        case .up:
            return self
        default:
            UIGraphicsBeginImageContextWithOptions(size, false, scale)
            draw(in: CGRect(origin: .zero, size: size))
            let result = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            return result
        }
    }
}
//let rotatedImage = image.rotate(radians: .pi)

extension UIProgressView {
    
    @IBInspectable var barHeight : CGFloat {
        get {
            return transform.d * 2.0
        }
        set {
            // 2.0 Refers to the default height of 2
            let heightScale = newValue / 2.0
            let c = center
            transform = CGAffineTransform(scaleX: 1.0, y: heightScale)
            center = c
        }
    }
}
// usage: let success = saveImage(image: UIImage(named: "image.png")!)
func saveImage(image: UIImage, fileName: String = "fileName") -> Bool {
    guard let fixOrientedImage = image.upOrientationImageBeforeSaveToLocalDisk() else {
        return false
    }
    guard let data = UIImageJPEGRepresentation(fixOrientedImage, 1) ?? UIImagePNGRepresentation(fixOrientedImage) else {
        return false
    }
    guard let directory = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false) as NSURL else {
        return false
    }
    do {
        try data.write(to: directory.appendingPathComponent(fileName + ".jpeg")!)
        return true
    } catch {
        print(error.localizedDescription)
        return false
    }
}
//usages for below: if let image = getSavedImage(named: "fileName") {
// do something with image
//}
func getSavedImage(named: String) -> UIImage? {
    if let dir = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false) {
        return UIImage(contentsOfFile: URL(fileURLWithPath: dir.absoluteString).appendingPathComponent(named + ".jpeg").path)
    }
    return nil
}
//usages for below: if  removeSavedImage(named: "fileName") == true {
// do something
//}
func removeSavedImage(named: String) -> Bool {

    if let dir = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false) {
        
        if FileManager.default.fileExists(atPath: URL(fileURLWithPath: dir.absoluteString).appendingPathComponent(named + ".jpeg").path) {
            try! FileManager.default.removeItem(atPath: URL(fileURLWithPath: dir.absoluteString).appendingPathComponent(named + ".jpeg").path)
            return true
        }
        
    }
    return false

}

func removeURLFile(url: URL) -> Bool {
    
    if FileManager.default.fileExists(atPath: url.absoluteString) {
        try! FileManager.default.removeItem(atPath: url.absoluteString)
        return true
    }
    return false
    
}

//// for localization
//// bundle extension
var bundleKey: UInt8 = 0

class AnyLanguageBundle: Bundle {
    
    override func localizedString(forKey key: String,
                                  value: String?,
                                  table tableName: String?) -> String {
        
        guard let path = objc_getAssociatedObject(self, &bundleKey) as? String,
            let bundle = Bundle(path: path) else {
                
                return super.localizedString(forKey: key, value: value, table: tableName)
        }
        
        return bundle.localizedString(forKey: key, value: value, table: tableName)
    }
}

extension Bundle {
    
    class func setLanguage(_ language: String) {
        
        defer {
            
            object_setClass(Bundle.main, AnyLanguageBundle.self)
        }
        
        objc_setAssociatedObject(Bundle.main, &bundleKey,    Bundle.main.path(forResource: language, ofType: "lproj"), .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
    }
}



// MARK: Localizable
public protocol Localizable {
    var localized: String { get }
}

extension String {

  var localized: String {
    return NSLocalizedString(self, comment: "\(self)_comment")
  }
  
  func localizedArgs(args: CVarArg...) -> String{
     return String(format: self.localized, arguments: args)
  }
}

//let siriCalendarText = "AnyCalendar"
//let localizedText = "LTo use Siri with my app, please set %@ as the default list on your device reminders settings".localizedArgs(args: siriCalendarTitle)



// MARK: XIBLocalizable
public protocol XIBLocalizable {
    var localeKey: String? { get set }
}

extension UILabel: XIBLocalizable {
    @IBInspectable public var localeKey: String? {
        get { return nil }
        set(key) {
            text = key?.localized
        }
    }
}

extension UIButton: XIBLocalizable {
    @IBInspectable public var localeKey: String? {
        get { return nil }
        set(key) {
            setTitle(key?.localized, for: .normal)
        }
    }
}

// MARK: Special protocol to localizaze UI's placeholder
public protocol UIPlaceholderXIBLocalizable {
    var localePlaceholderKey: String? { get set }
}

extension UITextField: UIPlaceholderXIBLocalizable {
    
    @IBInspectable public var localePlaceholderKey: String? {
        get { return nil }
        set(key) {
            placeholder = key?.localized
        }
    }
    
}

extension UISearchBar: UIPlaceholderXIBLocalizable {
    
    @IBInspectable public var localePlaceholderKey: String? {
        get { return nil }
        set(key) {
            placeholder = key?.localized
        }
    }
    
}


extension UITextField {
open override func awakeFromNib() {
        super.awakeFromNib()
    if UserDefaults.standard.value(forKey: "lang") as? String != nil && UserDefaults.standard.value(forKey: "lang") as? String == "2" {
            if textAlignment == .natural {
                self.textAlignment = .right
            }
        }
    }
}




//// end of for localization


/// prevent uiviewcontroller to be cardview

private func _swizzling(forClass: AnyClass, originalSelector: Selector, swizzledSelector: Selector) {
    if let originalMethod = class_getInstanceMethod(forClass, originalSelector),
       let swizzledMethod = class_getInstanceMethod(forClass, swizzledSelector) {
        method_exchangeImplementations(originalMethod, swizzledMethod)
    }
}

extension UIViewController {

    static let preventPageSheetPresentation: Void = {
        if #available(iOS 13, *) {
            _swizzling(forClass: UIViewController.self,
                       originalSelector: #selector(present(_: animated: completion:)),
                       swizzledSelector: #selector(_swizzledPresent(_: animated: completion:)))
        }
    }()

    @available(iOS 13.0, *)
    @objc private func _swizzledPresent(_ viewControllerToPresent: UIViewController,
                                        animated flag: Bool,
                                        completion: (() -> Void)? = nil) {
        if viewControllerToPresent.modalPresentationStyle == .pageSheet
                   || viewControllerToPresent.modalPresentationStyle == .automatic {
            viewControllerToPresent.modalPresentationStyle = .fullScreen
        }
        _swizzledPresent(viewControllerToPresent, animated: flag, completion: completion)
    }
}

extension UITabBarController {
    
    private struct AssociatedKeys {
        // Declare a global var to produce a unique address as the assoc object handle
        static var orgFrameView:     UInt8 = 0
        static var movedFrameView:   UInt8 = 1
    }
    
    var orgFrameView:CGRect? {
        get { return objc_getAssociatedObject(self, &AssociatedKeys.orgFrameView) as? CGRect }
        set { objc_setAssociatedObject(self, &AssociatedKeys.orgFrameView, newValue, .OBJC_ASSOCIATION_COPY) }
    }
    
    var movedFrameView:CGRect? {
        get { return objc_getAssociatedObject(self, &AssociatedKeys.movedFrameView) as? CGRect }
        set { objc_setAssociatedObject(self, &AssociatedKeys.movedFrameView, newValue, .OBJC_ASSOCIATION_COPY) }
    }
    
    override open func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        if let movedFrameView = movedFrameView {
            view.frame = movedFrameView
        }
    }
    
    func setTabBarVisible(visible:Bool, animated:Bool) {
        //since iOS11 we have to set the background colour to the bar color it seams the navbar seams to get smaller during animation; this visually hides the top empty space...
        view.backgroundColor =  self.tabBar.barTintColor
        // bail if the current state matches the desired state
        if (tabBarIsVisible() == visible) { return }
        
        //we should show it
        if visible {
            tabBar.isHidden = false
            UIView.animate(withDuration: animated ? 0.3 : 0.0) {
                //restore form or frames
                self.view.frame = self.orgFrameView!
                //errase the stored locations so that...
                self.orgFrameView = nil
                self.movedFrameView = nil
                //...the layoutIfNeeded() does not move them again!
                self.view.layoutIfNeeded()
            }
        }
            //we should hide it
        else {
            //safe org positions
            orgFrameView   = view.frame
            // get a frame calculation ready
            let offsetY = self.tabBar.frame.size.height
            movedFrameView = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height + offsetY)
            //animate
            UIView.animate(withDuration: animated ? 0.3 : 0.0, animations: {
                self.view.frame = self.movedFrameView!
                self.view.layoutIfNeeded()
            }) {
                (_) in
                self.tabBar.isHidden = true
            }
        }
    }
    
    func tabBarIsVisible() ->Bool {
        return orgFrameView == nil
    }
}

extension UIView {
    
    /**
     Rounds the given set of corners to the specified radius
     
     - parameter corners: Corners to round
     - parameter radius:  Radius to round to
     */
    func customRound(corners: UIRectCorner, radius: CGFloat) {
        _customRound(corners: corners, radius: radius)
    }
    
    /**
     Rounds the given set of corners to the specified radius with a border
     
     - parameter corners:     Corners to round
     - parameter radius:      Radius to round to
     - parameter borderColor: The border color
     - parameter borderWidth: The border width
     */
    func customRound(corners: UIRectCorner, radius: CGFloat, borderColor: UIColor, borderWidth: CGFloat) {
        let mask = _customRound(corners: corners, radius: radius)
        addBorder(mask: mask, borderColor: borderColor, borderWidth: borderWidth)
    }
    
    /**
     Fully rounds an autolayout view (e.g. one with no known frame) with the given diameter and border
     
     - parameter diameter:    The view's diameter
     - parameter borderColor: The border color
     - parameter borderWidth: The border width
     */
    func customFullyRound(diameter: CGFloat, borderColor: UIColor, borderWidth: CGFloat) {
        layer.masksToBounds = true
        layer.cornerRadius = diameter / 2
        layer.borderWidth = borderWidth
        layer.borderColor = borderColor.cgColor;
    }
    
    
    func addDashedBorder() {
      let color = UIColor.red.cgColor

      let shapeLayer:CAShapeLayer = CAShapeLayer()
      let frameSize = self.frame.size
      let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width, height: frameSize.height)

      shapeLayer.bounds = shapeRect
      shapeLayer.position = CGPoint(x: frameSize.width/2, y: frameSize.height/2)
      shapeLayer.fillColor = UIColor.clear.cgColor
      shapeLayer.strokeColor = color
      shapeLayer.lineWidth = 2
      shapeLayer.lineJoin = kCALineJoinRound
      shapeLayer.lineDashPattern = [6,3]
      shapeLayer.path = UIBezierPath(roundedRect: shapeRect, cornerRadius: 5).cgPath

      self.layer.addSublayer(shapeLayer)
      }
    
}

private extension UIView {
    
    @discardableResult func _customRound(corners: UIRectCorner, radius: CGFloat) -> CAShapeLayer {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
        return mask
    }
    
    func addBorder(mask: CAShapeLayer, borderColor: UIColor, borderWidth: CGFloat) {
        let borderLayer = CAShapeLayer()
        borderLayer.path = mask.path
        borderLayer.fillColor = UIColor.clear.cgColor
        borderLayer.strokeColor = borderColor.cgColor
        borderLayer.lineWidth = borderWidth
        borderLayer.frame = bounds
        layer.addSublayer(borderLayer)
    }
    
}

extension UIView {
    class func fromNib<T: UIView>() -> T {
        return Bundle(for: T.self).loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
}

extension UITableView {
    /**
     Calculates the total height of the tableView that is required if you ware to display all the sections, rows, footers, headers...
     */
    func getcontentHeight() -> CGFloat {
        var height = CGFloat(0)
        for sectionIndex in 0..<numberOfSections {
            //height += rectForSection(sectionIndex).size.height
            for row in 0..<numberOfRows(inSection: sectionIndex){
                //height += rectForSection(sectionIndex).size.height
                let indexPath = IndexPath(row: row, section: sectionIndex)
                height += rectForRow(at: indexPath).size.height
            }
        }
        return height
    }

}

extension UIApplication {
    class func topViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            let moreNavigationController = tab.moreNavigationController

            if let top = moreNavigationController.topViewController, top.view.window != nil {
                return topViewController(base: top)
            } else if let selected = tab.selectedViewController {
                return topViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        return base
    }
}

extension UITableView {
    func reloadData(completion: @escaping () -> ()) {
        UIView.animate(withDuration: 0, animations: { self.reloadData()})
        {_ in completion() }
    }
}

extension UICollectionView {
    func reloadData(completion: @escaping () -> ()) {
        UIView.animate(withDuration: 0, animations: { self.reloadData()})
        {_ in completion() }
    }
}

//example UIViewController.preventPageSheetPresentation

// load gif image
// https://stackoverflow.com/questions/27919620/how-to-load-gif-image-in-swift
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
/*
 // how to use
 #2 : Load GIF image Using Name

     let jeremyGif = UIImage.gifImageWithName("funny")
     let imageView = UIImageView(image: jeremyGif)
     imageView.frame = CGRect(x: 20.0, y: 50.0, width: self.view.frame.size.width - 40, height: 150.0)
     view.addSubview(imageView)
 #3 : Load GIF image Using Data

     let imageData = try? Data(contentsOf: Bundle.main.url(forResource: "play", withExtension: "gif")!)
     let advTimeGif = UIImage.gifImageWithData(imageData!)
     let imageView2 = UIImageView(image: advTimeGif)
     imageView2.frame = CGRect(x: 20.0, y: 220.0, width:
     self.view.frame.size.width - 40, height: 150.0)
     view.addSubview(imageView2)
 #4 : Load GIF image Using URL

     let gifURL : String = "http://www.gifbin.com/bin/4802swswsw04.gif"
     let imageURL = UIImage.gifImageWithURL(gifURL)
     let imageView3 = UIImageView(image: imageURL)
     imageView3.frame = CGRect(x: 20.0, y: 390.0, width: self.view.frame.size.width - 40, height: 150.0)
     view.addSubview(imageView3)
 */
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

extension UIImage {
    
    public class func gifImageWithData(_ data: Data) -> UIImage? {
        guard let source = CGImageSourceCreateWithData(data as CFData, nil) else {
            print("image doesn't exist")
            return nil
        }
        
        return UIImage.animatedImageWithSource(source)
    }
    
    public class func gifImageWithURL(_ gifUrl:String) -> UIImage? {
        guard let bundleURL:URL? = URL(string: gifUrl)
            else {
                print("image named \"\(gifUrl)\" doesn't exist")
                return nil
        }
        guard let imageData = try? Data(contentsOf: bundleURL!) else {
            print("image named \"\(gifUrl)\" into NSData")
            return nil
        }
        
        return gifImageWithData(imageData)
    }
    
    public class func gifImageWithName(_ name: String) -> UIImage? {
        guard let bundleURL = Bundle.main
            .url(forResource: name, withExtension: "gif") else {
                print("SwiftGif: This image named \"\(name)\" does not exist")
                return nil
        }
        guard let imageData = try? Data(contentsOf: bundleURL) else {
            print("SwiftGif: Cannot turn image named \"\(name)\" into NSData")
            return nil
        }
        
        return gifImageWithData(imageData)
    }
    
    class func delayForImageAtIndex(_ index: Int, source: CGImageSource!) -> Double {
        var delay = 0.1
        
        let cfProperties = CGImageSourceCopyPropertiesAtIndex(source, index, nil)
        let gifProperties: CFDictionary = unsafeBitCast(
            CFDictionaryGetValue(cfProperties,
                Unmanaged.passUnretained(kCGImagePropertyGIFDictionary).toOpaque()),
            to: CFDictionary.self)
        
        var delayObject: AnyObject = unsafeBitCast(
            CFDictionaryGetValue(gifProperties,
                Unmanaged.passUnretained(kCGImagePropertyGIFUnclampedDelayTime).toOpaque()),
            to: AnyObject.self)
        if delayObject.doubleValue == 0 {
            delayObject = unsafeBitCast(CFDictionaryGetValue(gifProperties,
                Unmanaged.passUnretained(kCGImagePropertyGIFDelayTime).toOpaque()), to: AnyObject.self)
        }
        
        delay = delayObject as! Double
        
        if delay < 0.1 {
            delay = 0.1
        }
        
        return delay
    }
    
    class func gcdForPair(_ a: Int?, _ b: Int?) -> Int {
        var a = a
        var b = b
        if b == nil || a == nil {
            if b != nil {
                return b!
            } else if a != nil {
                return a!
            } else {
                return 0
            }
        }
        
        if a < b {
            let c = a
            a = b
            b = c
        }
        
        var rest: Int
        while true {
            rest = a! % b!
            
            if rest == 0 {
                return b!
            } else {
                a = b
                b = rest
            }
        }
    }
    
    class func gcdForArray(_ array: Array<Int>) -> Int {
        if array.isEmpty {
            return 1
        }
        
        var gcd = array[0]
        
        for val in array {
            gcd = UIImage.gcdForPair(val, gcd)
        }
        
        return gcd
    }
    
    class func animatedImageWithSource(_ source: CGImageSource) -> UIImage? {
        let count = CGImageSourceGetCount(source)
        var images = [CGImage]()
        var delays = [Int]()
        
        for i in 0..<count {
            if let image = CGImageSourceCreateImageAtIndex(source, i, nil) {
                images.append(image)
            }
            
            let delaySeconds = UIImage.delayForImageAtIndex(Int(i),
                source: source)
            delays.append(Int(delaySeconds * 1000.0)) // Seconds to ms
        }
        
        let duration: Int = {
            var sum = 0
            
            for val: Int in delays {
                sum += val
            }
            
            return sum
        }()
        
        let gcd = gcdForArray(delays)
        var frames = [UIImage]()
        
        var frame: UIImage
        var frameCount: Int
        for i in 0..<count {
            frame = UIImage(cgImage: images[Int(i)])
            frameCount = Int(delays[Int(i)] / gcd)
            
            for _ in 0..<frameCount {
                frames.append(frame)
            }
        }
        
        let animation = UIImage.animatedImage(with: frames,
            duration: Double(duration) / 1000.0)
        
        return animation
    }
}
// end of load gif image





