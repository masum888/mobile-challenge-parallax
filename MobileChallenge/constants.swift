//
//  constants.swift
//  intShop
//
//  Created by Ashraf Ul Alam Tusher on 11/27/16.
//  Copyright © 2016 Aix Systems. All rights reserved.
//

import Foundation
import UIKit
import AlamofireImage


let Base_Url = "https://virtserver.swaggerhub.com/m-tul/opn-mobile-challenge-api/1.0.0/"

let Success_Code = 200
let FAIL_CODE = 400
let SUCCESS_CODE = 200

typealias DownloadComplete = () -> ()
typealias backgroundTaskCompleted = () -> ();


// Error Texts
let network_connection_error = "Could not connect to the server, please try again."

let no_internet_connection = "You are offline, please connect to internet."

let error_title_english = "Error!!"

let success_title_english = "Successful!!"


// indicators
var isComingfromAccountVC = false
var isComingfromRrestaurantVC = false
var Error_connecting = "Network Error"


let deviceID = UIDevice.current.identifierForVendor!.uuidString




//MC

let mcObject = MCNetworkCalls()

var storeInfo = MCStoreInfo()
var productList = MCProductList()
var selectedProductList = [MCProduct]()




