//
//  NSBundle+Language.h
//  Dutabash
//
//  Created by Masum Ahmed on 25/1/20.
//  Copyright © 2020 Ashraf Ul Alam Tusher. All rights reserved.
//

#ifndef NSBundle_Language_h
#define NSBundle_Language_h


#endif /* NSBundle_Language_h */

#import <Foundation/Foundation.h>

@interface NSBundle (Language)
+(void)setLanguage:(NSString*)language;

@end
