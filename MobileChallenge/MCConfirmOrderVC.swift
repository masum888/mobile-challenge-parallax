//
//  MCConfirmOrderVC.swift
//  MobileChallenge
//
//  Created by Masum Ahmed on 16/8/21.
//  Copyright © 2021 Ashraf Ul Alam Tusher. All rights reserved.
//

import UIKit
import SwiftEntryKit

class MCConfirmOrderVC: MasterViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var productListTV: IntrinsicTableView!
    @IBOutlet weak var deliveryAddressTxt: UITextField!
    @IBOutlet weak var totalAmountLbl: UILabel!
    @IBOutlet weak var confirmView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        productListTV.dataSource = self
        productListTV.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.selectedItemChanged(notification:)), name: NSNotification.Name(rawValue: "selectedItemChanged"), object: nil)

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.productListTV.reloadData()
        deliveryAddressTxt.text = ""
        
        
        self.navigationController?.isNavigationBarHidden = false
        
        calculateTotalAmount()
        confirmView.isHidden = true
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        self.navigationController?.isNavigationBarHidden = true
    }
    
    @IBAction func confirmOrderBtnPressed(_ sender: Any) {
        
        if deliveryAddressTxt.text?.isEmpty == true {
            
            MasAlertViewManager.showAlert(target: self, animated: true, message: "Please enter delivery address.")
        }else if selectedProductList.count == 0 {
            
            MasAlertViewManager.showAlert(target: self, animated: true, message: "Please select atleast one product to confirm order.")
        }else{
            
            let progressHUD = ProgressHUD(text: "Loading...")
            self.view.addSubview(progressHUD)
            progressHUD.show()
            
            mcObject.postOrder(delivery_address: deliveryAddressTxt.text ?? "", products: selectedProductList, success: { (status, message) in
                
                print(status)
                print(message)
                
                progressHUD.hide()
                
                if status == 200 {
                    
                    self.navigationController?.isNavigationBarHidden = true
                    self.confirmView.isHidden = false

                } else if status == 100 {

                    MasAlertViewManager.showAlert(target: self, animated: true, message: message)
                    
                }
                
            }) { (message) in

                progressHUD.hide()
                // Set note label content
                let text = message
                let style = EKProperty.LabelStyle(font: MainFont.light.with(size: 14), color: .white, alignment: .center)
                let labelContent = EKProperty.LabelContent(text: text, style: style)
                let imageContent = EKProperty.ImageContent(image: UIImage(named: "ic_wifi")!)
                let contentView = EKImageNoteMessageView(with: labelContent, imageContent: imageContent)
                SwiftEntryKit.display(entry: contentView, using: self.dataSource[1, 2].attributes)
                
            }
            
            
        }
        
        
        
    }
    
    @IBAction func goToFirstPageBtnPressed(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func calculateTotalAmount()
    {
        if selectedProductList.count > 0 {
            var total : Double = 0
            for a in selectedProductList {
                
                //let price = 0
                let dqty = Double( productList.list?.filter({ $0.name == a.name }).first?.qty ?? 1 )
                let dprice = Double( productList.list?.filter({ $0.name == a.name }).first?.price ?? 0  )
                let totalp = dqty * dprice
                //a.price = totalp
                
                total = total + (totalp)
                
            }
            totalAmountLbl.text = "\(total)"
        }else{
            totalAmountLbl.text = "0"
        }
    }
    
    @objc func selectedItemChanged(notification: Notification){
        calculateTotalAmount()
        print("selectedProductList.count",selectedProductList.count)
    }
    
    
    
    
    //MARK:- TableView
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return selectedProductList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MCProductListTVCell", for: indexPath) as! MCProductListTVCell
        cell.index = indexPath.row
        cell.updateCellWith(row: selectedProductList[indexPath.row])
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {

    }
    



}
